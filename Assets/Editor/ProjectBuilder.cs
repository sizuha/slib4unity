﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEditor.iOS.Xcode.PBX;
using UnityEditor.iOS.Xcode;
using PlistCS;


class ProjectBuilder {
	//static string[] SCENES = FindEnabledEditorScenes();
	static string TARGET_DIR = "Build";

	// Outputs
	const string DMG_OUTPUT_NAME = "SLib Sample";
	const string DMG_VOLUMN_NAME = "SLib Sample";

	const string UNITY_DEFAULT_DEFINES = "USING_LEAN_TWEEN";
	static string UNITY_DEFINES;

	private static string[] FindEnabledEditorScenes() {
		List<string> EditorScenes = new List<string>();
		foreach(EditorBuildSettingsScene scene in EditorBuildSettings.scenes) {
			if (!scene.enabled) continue;
			EditorScenes.Add(scene.path);
		}
		return EditorScenes.ToArray();
	}

	static void createBuildDirs() {
		var build_path = Application.dataPath + "/../" + TARGET_DIR;

		UnityEngine.Debug.Log("createBuildDirs..");
		Directory.CreateDirectory(build_path + "/ios");
		Directory.CreateDirectory(build_path + "/android");
		Directory.CreateDirectory(build_path + "/macOS/package");
		Directory.CreateDirectory(build_path + "/win");
	}

	static void GenericBuild(string[] scenes, string target_filename, BuildTarget build_target, BuildOptions build_options) {
		createBuildDirs();

		EditorUserBuildSettings.SwitchActiveBuildTarget(build_target);
		string res = BuildPipeline.BuildPlayer(scenes, target_filename, build_target, build_options);
		if (res.Length > 0) {
			throw new Exception("BuildPlayer failure: " + res);
		}
	}

	[PostProcessBuild()]
	public static void OnPostprocessBuild(BuildTarget target, string pathToBuildProject) {        
		var out_dir = Path.GetDirectoryName(pathToBuildProject);
		UnityEngine.Debug.Log("----Custome Script---Executing post process build phase.");
		UnityEngine.Debug.Log("Application.dataPath: " + Application.dataPath);
		UnityEngine.Debug.Log("pathToBuildProject: " + pathToBuildProject);
		UnityEngine.Debug.Log("output path: " + out_dir);

		if (target == BuildTarget.StandaloneOSXIntel64) {
			var dmg_name = Application.dataPath + "/../" + TARGET_DIR;

			dmg_name += "/macOS/" + DMG_OUTPUT_NAME;

			Process myCustomProcess = new Process();
			myCustomProcess.StartInfo.FileName = Application.dataPath + "/Editor/mk_pkg_osx";
			myCustomProcess.StartInfo.Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\"", out_dir+"/", dmg_name, DMG_VOLUMN_NAME);
			myCustomProcess.StartInfo.UseShellExecute = true;
			myCustomProcess.StartInfo.RedirectStandardOutput = false;
			myCustomProcess.Start();

			UnityEngine.Debug.Log("make a DMG.");
			myCustomProcess.WaitForExit();
		}
		if (target == BuildTarget.iOS) {
			string projPath = PBXProject.GetPBXProjectPath(pathToBuildProject);
			PBXProject proj = new PBXProject();

			proj.ReadFromString(File.ReadAllText(projPath));
			string xcode_target = proj.TargetGuidByName("Unity-iPhone");
			//proj.SetBuildProperty(xcode_target, "PRODUCT_BUNDLE_IDENTIFIER", "com.styly.stylypocket");

			// システムのフレームワークを追加
			//proj.AddFrameworkToProject(xcode_target, "CoreLocation.framework", false);
			//proj.AddFrameworkToProject(xcode_target, "CoreBluetooth.framework", false);
			//proj.AddFrameworkToProject(xcode_target, "Speech.framework", true); // set optional
			Xcode_addLibraryToFrameworks(proj, xcode_target, "libz.1.2.5.tbd");// for ZIP


			// フレームワークを追加 --------
			//proj.AddFileToBuild(xcode_target, proj.AddFile("Frameworks/Lib_ios/OpenEars.framework", "Frameworks/Lib_ios/OpenEars.framework", PBXSourceTree.Source));
			//proj.AddFileToBuild(xcode_target, proj.AddFile("Frameworks/Lib_ios/Slt.framework", "Frameworks/Lib_ios/Slt.framework", PBXSourceTree.Source));

			//--------------------------------

			// ファイルを追加
			//var fileName = "my_file.xml";
			//var filePath = Path.Combine("Assets/Lib", fileName);
			//File.Copy(filePath, Path.Combine(pathToBuildProject, fileName));
			//proj.AddFileToBuild(xcode_target, proj.AddFile(fileName, fileName, PBXSourceTree.Source));

			// Yosemiteでipaが書き出せないエラーに対応するための設定
			//proj.SetBuildProperty(xcode_target, "CODE_SIGN_RESOURCE_RULES_PATH", "$(SDKROOT)/ResourceRules.plist");

			// フレームワークの検索パスを設定・追加
			proj.SetBuildProperty(xcode_target, "FRAMEWORK_SEARCH_PATHS", "$(inherited)");
			proj.AddBuildProperty(xcode_target, "FRAMEWORK_SEARCH_PATHS", "$(PROJECT_DIR)/Frameworks");
			//proj.AddBuildProperty(xcode_target, "FRAMEWORK_SEARCH_PATHS", "$(PROJECT_DIR)/Frameworks/Lib_ios");

			// 書き出し
			File.WriteAllText(projPath, proj.WriteToString());

			//--- update: Info.plist ---
			var info_plist_path = pathToBuildProject + "/Info.plist";
			{
				var plist = (Dictionary<string, object>)Plist.readPlist(info_plist_path);

				//var appQueriesSchemes = new List<object>();
				//plist["LSApplicationQueriesSchemes"] = appQueriesSchemes;
				//appQueriesSchemes.Add("line");

				//plist.Add("NSCameraUsageDescription", "for QRCode");

				//plist.Add("NSLocationAlwaysUsageDescription", "for iBeacon");
				//plist.Add("NSLocationWhenInUseUsageDescription", "for iBeacon");


				// for show/hide iOS Status Bar
				plist.Add("UIViewControllerBasedStatusBarAppearance", false);

				Plist.writeXml(plist, info_plist_path);
			}
		}

		UnityEngine.Debug.Log("----Custome Script--- Finished executing post process build phase.");
	}

	private static void Xcode_addLibraryToFrameworks(PBXProject proj, string targetGuid, string lib_name) {
		string fileGuid = proj.AddFile("usr/lib/"+lib_name, "Frameworks/"+lib_name, PBXSourceTree.Sdk);
		proj.AddFileToBuild(targetGuid, fileGuid);
	}

	internal static void CopyAndReplaceDirectory(string srcPath, string dstPath)
	{
		if (Directory.Exists(dstPath))
			Directory.Delete(dstPath);
		if (File.Exists(dstPath))
			File.Delete(dstPath);

		Directory.CreateDirectory(dstPath);

		foreach (var file in Directory.GetFiles(srcPath))
			File.Copy(file, Path.Combine(dstPath, Path.GetFileName(file)));

		foreach (var dir in Directory.GetDirectories(srcPath))
			CopyAndReplaceDirectory(dir, Path.Combine(dstPath, Path.GetFileName(dir)));
	}

	private static BuildOptions ApplyDefines(bool debug_mode) {
		var build_options = BuildOptions.None;
		if (debug_mode) {
			UNITY_DEFINES += ";DEBUG";
			build_options = BuildOptions.Development;
		}
		else {
			UNITY_DEFINES += ";RELEASE";
		}

		return build_options;
	}

	private static void ResetDefines() {
		UNITY_DEFINES = UNITY_DEFAULT_DEFINES;
	}


	//--- Android ------------------------------------------------------------------------

	[MenuItem ("Build/Release/Android")]
	static void PerformAndroidBuild_release() {
		PerformAndroidBuild(false);
	}

	static void PerformAndroidBuild(bool debug_mode, bool is_trial_build = false) {
		ResetDefines();
		string target_filename = null;

		target_filename = TARGET_DIR + "/android/";
		target_filename += PlayerSettings.productName;
		PlayerSettings.Android.keyaliasPass = "testtest"; // TODO Change

		var build_options = ApplyDefines(debug_mode);
		PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, UNITY_DEFINES);

		GenericBuild(FindEnabledEditorScenes(), target_filename, BuildTarget.Android, build_options);
	}

	//--- iOS ------------------------------------------------------------------------

	[MenuItem ("Build/Release/iOS")]
	static void PerformIOSBuild_release() {
		PerformIOSBuild(false);
	}

	static void PerformIOSBuild(bool debug_mode) {
		ResetDefines();
		string target_filename = null;

		target_filename = TARGET_DIR + "/ios/";
		target_filename += PlayerSettings.productName;

		var build_options = ApplyDefines(debug_mode);
		PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, UNITY_DEFINES);
		GenericBuild(FindEnabledEditorScenes(), target_filename, BuildTarget.iOS, build_options);
	}

	//--- OS X ------------------------------------------------------------------------

	[MenuItem ("Build/Release/Mac OS X")]
	static void PerformOSXBuild_release() {
		PerformOSXBuild(false);
	}

	static void PerformOSXBuild(bool debug_mode) {
		ResetDefines();
		string target_filename;

		target_filename = TARGET_DIR + "/mscOS/package/";
		target_filename += PlayerSettings.productName;

		var build_options = ApplyDefines(debug_mode);
		PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, UNITY_DEFINES);
		GenericBuild(FindEnabledEditorScenes(), target_filename, BuildTarget.StandaloneOSXIntel64, build_options);
	}

	//--- Win32 ------------------------------------------------------------------------

	[MenuItem ("Build/Release/Win32")]
	static void PerformWinBuild_release() {
		PerformWinBuild(false);
	}

	static void PerformWinBuild(bool debug_mode) {
		ResetDefines();
		string target_filename;

		target_filename = TARGET_DIR + "/win/";	
		target_filename += PlayerSettings.productName + ".exe";

		var build_options = ApplyDefines(debug_mode);
		PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, UNITY_DEFINES);
		GenericBuild(FindEnabledEditorScenes(), target_filename, BuildTarget.StandaloneWindows, build_options);
	}


}