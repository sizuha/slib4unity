﻿using UnityEngine;
using System.Collections;

namespace SLib {
	namespace Media {

		public class SoundPlayer : MonoBehaviour {

			public string soundID;
			AudioSource audioSrc;

			void Awake() {
				if (string.IsNullOrEmpty(soundID)) {
					audioSrc = GetComponent<AudioSource>();	
				}
			}

			public void play() {
				if (audioSrc) {
					if (App.instance.enableSound) {
						audioSrc.Play();
					}
				}
				else if (!string.IsNullOrEmpty(soundID)) {
					GlobalSoundPlayer.play(soundID);
				}
			}

		}

	}
}