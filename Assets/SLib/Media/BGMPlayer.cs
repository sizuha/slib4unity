﻿using UnityEngine;
using System.Collections;

namespace SLib {
	namespace Media {

		[RequireComponent(typeof(AudioSource))]
		public class BGMPlayer : MonoBehaviour {

			public bool playOnAwake = true;

			AudioSource audioSrc;

			void Awake() {
				audioSrc = GetComponent<AudioSource>();
			}

			void OnEnable() {
				if (playOnAwake) {
					if (App.instance.enableBGM) {
						audioSrc.Play();
					}
				}
			}

			public void play() {
				if (App.instance.enableBGM && !audioSrc.isPlaying) {
					audioSrc.Play();
				}
			}

			public void pause() {
				if (audioSrc) audioSrc.Pause();
			}

			public void stop() {
				if (audioSrc) audioSrc.Stop();
			}

			void OnDestroy() {
				stop();
			}

		}

	}
}