﻿using UnityEngine;
using System.Collections;

namespace SLib {
	namespace Media {

		public class GlobalSoundPlayer : MonoBehaviour {

			static GlobalSoundPlayer __instance;

			void Awake() {
				__instance = this;
			}

			// return: length of a audio-clip. (seconds)
			public static float play(string sound_id, bool not_mute = false) {
				UnityEngine.Debug.Log("find sound: " + sound_id);

				var sound = __instance.transform.FindChild(sound_id).GetComponent<AudioSource>();

				if (App.instance.enableSound || not_mute) {
					sound.Play();
				}

				return sound.clip.length;
			}

			public static AudioSource play2(string sound_id, bool not_mute = false) {
				var sound = __instance.transform.FindChild(sound_id).GetComponent<AudioSource>();
				
				if (App.instance.enableSound || not_mute) {
					sound.Play();
				}
				
				return sound;
			}

			public static void playFromFile(AudioSource player, string snd_path) {
				__instance.StartCoroutine( __instance.playByUrl(player, "file://" + snd_path) );
			}

			IEnumerator playByUrl(AudioSource player, string url) {
				using (var www = new WWW(url)) {
					yield return www;

					var clip = www.GetAudioClip(false);
					player.clip = clip;
					player.Play();
				}

				yield return new WaitForSeconds(player.clip.length);
			}

		}

	}
}