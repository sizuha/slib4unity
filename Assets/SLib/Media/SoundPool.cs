﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace SLib {
	namespace Media {

		public class SoundPool {

			Dictionary<string, AudioClip> sound_pool;

			public SoundPool() {
				sound_pool = new Dictionary<string, AudioClip>();
			}

			public void release() {
				foreach (AudioClip clip in sound_pool.Values) {
					Resources.UnloadAsset(clip);
				}

				sound_pool.Clear();
			}

			public AudioClip loadFromResource(string path, string snd_key = null) {
				if (string.IsNullOrEmpty(snd_key)) {
					snd_key = path;
				}

				if (sound_pool.ContainsKey(snd_key)) {
					return sound_pool[snd_key];
				}

				var clip = Resources.Load<AudioClip>(path);
				sound_pool.Add(snd_key, clip);
				return clip;
			}

			public void load(AudioClip clip, string snd_key = null) {
				sound_pool.Add(snd_key, clip);
			}

			public AudioClip getSound(string snd_key) {
				if (sound_pool.ContainsKey(snd_key)) {
					return sound_pool[snd_key];
				}

				return null;
			}

			public void play(string snd_key, AudioSource src) {
				src.clip = sound_pool[snd_key];
				src.Play();
			}

		}

	}
}