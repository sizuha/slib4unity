﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace SLib {

	public class App : MonoBehaviour {

		public GameObject entryPrefab;
		public Vector2 standaloneResolution = new Vector2(1024, 578); 
		public string UIRootTag = "UI_ROOT";

		public bool enableSound = true;
		public bool enableBGM = true;

		bool initialized = false;

		private static App __instance;
		public static App instance {
			get {
				if (!__instance) {
					__instance = GameObject.FindObjectOfType<App>();
					DontDestroyOnLoad(__instance);
				}

				return __instance;
			}
		}

		private RectTransform ui_root;
		public RectTransform getUIRoot() {
			if (!ui_root) {
				ui_root = GameObject.FindGameObjectWithTag(UIRootTag).GetComponent<RectTransform>();
			}

			return ui_root;
		}


		void Awake() {
			if (initialized) {
				Destroy(gameObject);
				return;
			}

			__instance = this;
			DontDestroyOnLoad(__instance);

			#if UNITY_STANDALONE
			Screen.SetResolution((int)standaloneResolution.x, (int)standaloneResolution.y, false);
			#endif

			Application.targetFrameRate = 30;
			Screen.fullScreen = false;
			Input.multiTouchEnabled = false;

			initialized = true;
		}

		IEnumerator Start() {
			if (entryPrefab) {
				yield return null;

				#if !UNITY_ANDROID
				yield return new WaitForSeconds(1f);
				#endif

				SLib.Prefab.loadToUIRoot(entryPrefab);
			}
		}

		public void quit() {
			Application.Quit();
		}

		public static void clearUnused() {		
			Resources.UnloadUnusedAssets();
			System.GC.Collect();
		}

	}

} // end of namespace