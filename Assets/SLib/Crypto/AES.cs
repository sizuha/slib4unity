﻿#if USING_CRYPTO
using System.Security.Cryptography;
#endif

using System.Text;
using System;
using UnityEngine;


namespace SLib {
	namespace Crypto {

		public class AES {

			private string iv;

			public AES(string key_16len) {
				#if !UNITY_WEBPLAYER && USING_CRYPTO
				if (key_16len.Length < 16) {
					throw new System.Security.Cryptography.CryptographicException("key length error: key_16len < 16");
				}

				iv = key_16len.Substring(0, 16);
				#endif
			}

			private static string key {
				get {
					return UnityEngine.Application.bundleIdentifier.Substring(0,6) + SystemInfo.deviceUniqueIdentifier.Substring(0,10); 
				}
			}

			/// <summary>
			/// 文字列をAESで暗号化.
			/// </summary>
			public string Encrypt(string text) {
				#if !UNITY_WEBPLAYER && USING_CRYPTO
				// AES暗号化サービスプロバイダ.
				AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
				aes.BlockSize = 128;
				aes.KeySize = 128;

				aes.IV = Encoding.UTF8.GetBytes(iv);
				aes.Key = Encoding.UTF8.GetBytes(key);
				aes.Mode = CipherMode.CBC;
				aes.Padding = PaddingMode.PKCS7;

				// 文字列をバイト型配列に変換.
				byte[] src = Encoding.Unicode.GetBytes(text);

				// 暗号化する.
				using (ICryptoTransform encrypt = aes.CreateEncryptor())
				{
					byte[] dest = encrypt.TransformFinalBlock(src, 0, src.Length);

					// バイト型配列からBase64形式の文字列に変換.
					return Convert.ToBase64String(dest);
				}
				#else
				return text;
				#endif
			}

			/// <summary>
			/// 文字列をAESで復号化.
			/// </summary>
			public string Decrypt(string text) {
				#if !UNITY_WEBPLAYER && USING_CRYPTO
				// AES暗号化サービスプロバイダ.
				AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
				aes.BlockSize = 128;
				aes.KeySize = 128;
				aes.IV = Encoding.UTF8.GetBytes(iv);
				aes.Key = Encoding.UTF8.GetBytes(key);
				aes.Mode = CipherMode.CBC;
				aes.Padding = PaddingMode.PKCS7;

				// Base64形式の文字列からバイト型配列に変換.
				byte[] src = System.Convert.FromBase64String(text);

				// 複号化する.
				using (ICryptoTransform decrypt = aes.CreateDecryptor())
				{
					byte[] dest = decrypt.TransformFinalBlock(src, 0, src.Length);
					return Encoding.Unicode.GetString(dest);
				}
				#else
				return text;
				#endif
			}
		}
			
	}
}
