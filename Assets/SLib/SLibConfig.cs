﻿//#define USING_LEAN_TWEEN
//#define USING_CRYPTO

namespace SLib {

	public class Config {

		public class Android {
			public const string PLUGIN_PKG = "plugins.unity.sizuha.util";

			public const string PLUGIN_UTIL_CLASS = PLUGIN_PKG + ".UnityUtil";
			public const string PLUGIN_WEBVIEW_CLASS = PLUGIN_PKG + ".WebViewPlugin";
		}

	}

}