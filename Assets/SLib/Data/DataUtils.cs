﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using System;
using System.Globalization;

namespace SLib {
	namespace Data {

		public class Util {

			public static int parseToInt(string src, int defVal = 0) {
				if (string.IsNullOrEmpty(src)) return defVal;

				var result = defVal;
				if (!int.TryParse(src, out result)) return defVal;

				return result;
			}
			
			public static float parseToFloat(string src, float defVal = 0) {
				if (string.IsNullOrEmpty(src)) return defVal;

				var result = defVal;
				if (!float.TryParse(src, out result)) return defVal;

				return result;
			}

			public static double parseToDouble(string src, double defVal = 0) {
				return string.IsNullOrEmpty(src) ? defVal : double.Parse(src);
			}

			public static string arrayToString(int[] arrs) {
				string result = "";

				if (arrs != null) {
					bool first = true;
					foreach (int i in arrs) {
						result += (first ? "" : ",") + i.ToString();

						if (first) first = false;
					}
				}

				return result;
			}

			public static bool isValidEmail(string email) {
				return Regex.IsMatch(email, "(\\w+\\.)*\\w+@(\\w+\\.)+[A-Za-z]+");
			}

			public static Color ParseHexColor(string hexstring) {
				if (hexstring.StartsWith("#")) {
					hexstring = hexstring.Substring(1);
				}
				
				if (hexstring.StartsWith("0x")) {
					hexstring = hexstring.Substring(2);
				}
				
				byte a = 1;
				
				if (hexstring.Length == 8)  {
					a = byte.Parse(hexstring.Substring(6, 2), NumberStyles.HexNumber);
				}
				else {
					throw new Exception(string.Format("{0} is not a valid color string.", hexstring));
				}

				byte r = byte.Parse(hexstring.Substring(0, 2), NumberStyles.HexNumber);
				byte g = byte.Parse(hexstring.Substring(2, 2), NumberStyles.HexNumber);
				byte b = byte.Parse(hexstring.Substring(4, 2), NumberStyles.HexNumber);

				return new Color32(r, g, b, a);
			}

		}

	}
}