﻿using System;
using SimpleJSON;

namespace SLib {
	namespace Data {

		public class Json {

			public static string loadString(JSONNode jsonNode, string defVal = null) {
				if (jsonNode == null || jsonNode.Value == null)
					return defVal;

				return jsonNode.Value;
			}

			public static int loadInt(JSONNode jsonNode, int defVal = 0) {
				if (jsonNode == null || jsonNode.Value == null)
					return defVal;

				return jsonNode.AsInt;
			}

		}

	}
}