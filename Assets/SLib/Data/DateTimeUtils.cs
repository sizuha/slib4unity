﻿using System.Collections;
using System;

namespace SLib {
	namespace Data {

		public class DateTimeUtil {

			private static readonly DateTime UnixEpoch =
				new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			
			public static long GetCurrentUnixTimestampMillis()
			{
				return (long) (DateTime.UtcNow - UnixEpoch).TotalMilliseconds;
			}
			
			public static DateTime DateTimeFromUnixTimestampMillis(long millis)
			{
				return UnixEpoch.AddMilliseconds(millis);
			}
			
			public static long GetCurrentUnixTimestampSeconds()
			{
				return (long) (DateTime.UtcNow - UnixEpoch).TotalSeconds;
			}
			
			public static DateTime DateTimeFromUnixTimestampSeconds(long seconds)
			{
				return UnixEpoch.AddSeconds(seconds);
			}

			// 01234567890123
			// YYYYMMDDhhmmss
			public static DateTime DateTimeFromString(string source) {
				try {
					var year = int.Parse(source.Substring(0,4));
					var month = int.Parse(source.Substring(4,2));
					var day = int.Parse(source.Substring(6,2));

					var hour = int.Parse(source.Substring(8,2));
					var min = int.Parse(source.Substring(10,2));
					var sec = int.Parse(source.Substring(12,2));

					return new DateTime(year, month, day, hour, min, sec);
				}
				catch (Exception e) {
					return new DateTime();
				}
			}

			public static string DateTimeToStr(DateTime dateTime) {
				return string.Format("{0:0000}{1:00}{2:00}{3:00}{4:00}{5:00}", 
				                     dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second);
			}

		}

	}
}