﻿using System;
using System.IO;

namespace SLib {
	namespace IO {
		
		public class File {

			public static byte[] ReadAllBytes(string fileName)
			{
				byte[] buffer = null;
				using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
				{
					buffer = new byte[fs.Length];
					fs.Read(buffer, 0, (int)fs.Length);
				}
				return buffer;
			} 

			public static byte[] ReadAllBytes(FileStream fs)
			{
				byte[] buffer = null;
				using (fs)
				{
					buffer = new byte[fs.Length];
					fs.Read(buffer, 0, (int)fs.Length);
				}
				return buffer;
			} 

		}

	}
}

