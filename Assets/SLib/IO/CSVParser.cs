﻿using UnityEngine;
using System.Collections;
using System.IO;

namespace SLib {
	namespace IO {

		public class CSVParser {
			public int skip_lines = 1;
			public int max_cols = 0;
			
			private System.Action<int,string> onLine = null; // param: int row_idx, string line
			private System.Action<int> onLineEnd = null; // param: int row_idx

			public delegate bool Event_OnReadColumn(int row_idx, int col_idx, string col);
			private Event_OnReadColumn onColumn = null;

			public CSVParser() {}

			public void setOnReadLine(System.Action<int,string> onReadLine) {
				this.onLine = onReadLine;
			}

			public void setOnLineEnd(System.Action<int> onLineEnd) {
				this.onLineEnd = onLineEnd;
			}

			public void setOnReadColumn(Event_OnReadColumn onReadCol) {
				this.onColumn = onReadCol;
			}

			public void parseFromTextAsset(string file_path, bool unload_res = true) {
				TextAsset src = (TextAsset)Resources.Load(file_path,  typeof(TextAsset));
				parseFromTextAsset(src, unload_res);
			}
			
			public void parseFromTextAsset(TextAsset csv_file, bool unload_res = true) {
				parse(csv_file.text);
				
				if (unload_res) {
					Resources.UnloadAsset(csv_file);
				}
			}
			
			public void parse(string csv_text) {
				StringReader r = new StringReader(csv_text);
				parse(r);
			}
			
			public void parse(TextReader r, bool auto_close = true) {
				string line = null;
				int row_idx = 0;
				bool break_parsing = false;
				
				do {
					line = r.ReadLine();
					if (line == null) break;
					
					if (row_idx < this.skip_lines) {
						row_idx++;
						continue;
					}
					
					if (this.onLine != null) {
						this.onLine(row_idx, line);
					}
					
					string[] cols = line.Split(',');
					int col_idx = 0;

					bool flag_quote_begin = false;
					string col_str = "";
					
					foreach (string col in cols) {
						if (this.max_cols > 0 && col_idx > this.max_cols) break;

						if (flag_quote_begin) {
							if (col.EndsWith("\"")) {
								flag_quote_begin = false;
								col_str += "," + col.Substring(0, col.Length-1);
							}
							else {
								col_str += "," + col;
								continue;
							}
						}
						else if (col.StartsWith("\"")) {
							if (col.EndsWith("\"")) {
								col_str = col.Substring(1, col.Length-2);
							}
							else {
								flag_quote_begin = true;
								col_str = col.Substring(1, col.Length-1);
								continue;
							}
						}
						else {
							col_str = col;
						}

						if (col_str.Contains("\"\"")) {
							col_str = convertDoubleQuote(col_str);
							//Debug.Log("!!!!" + col_str);
						}

						if (this.onColumn != null) {
							if (!this.onColumn(row_idx, col_idx, col_str)) {
								break_parsing = true;
								break;
							}
						}
						col_idx++;
					}

					if (this.onLineEnd != null) {
						this.onLineEnd(row_idx);
					}
					row_idx++;

					if (break_parsing) break;
				} while (line != null);

				if (auto_close) {
					r.Close();
				}
			}

			string convertDoubleQuote(string source) {
				string result = "";
				char prev_c = (char)0;

				foreach (var c in source) {
					if (prev_c == '"' && c == '"') {
						prev_c = (char)0;
						continue;
					}

					prev_c = c;
					result += c;
				}

				return result;
			}
		}
	
	}
}