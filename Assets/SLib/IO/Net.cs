﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SLib {
	namespace IO {

		public class Net {

			public abstract class ServerAPI : MonoBehaviour {

				abstract protected string getBaseUrl();
				virtual protected bool isEnableLog() { return true; }

				public struct RESULT_CODES {
					public const int OK = 1;
					public const int ERROR = 0;
				}


				private static ServerAPI __instance = null;
				protected static ServerAPI instance {
					get {
						if (!__instance) {
							__instance = GameObject.FindObjectOfType<ServerAPI>();
						}
						return __instance;
					}
				}

				void Awake() {
					if (!__instance) {
						__instance = this;
					}
				}

				public void HttpPost_raw(string url, WWWForm param, System.Action<WWW> response = null)  {
					if (url != null) {
						StartCoroutine(postConnect(url, param, response));
					}
				}

				public void HttpGet_raw(string url, System.Action<WWW> response = null) {
					if (url != null) {
						StartCoroutine(connect(url, response));
					}
				}

				public void HttpPost(string url, WWWForm param, System.Action<WWW> response = null)  {
					if (url != null) {
						StartCoroutine(postConnect(getBaseUrl() + url, param, response));
					}
				}

				public void HttpGet(string url, System.Action<WWW> response = null) {
					if (url != null) {
						StartCoroutine(connect(getBaseUrl() + url, response));
					}
				}

				IEnumerator postConnect(string url, WWWForm param, System.Action<WWW> response = null) {
					var www = (param != null) ? new WWW(url, param) : new WWW(url);
					yield return www;

					if (isEnableLog()) {
						UnityEngine.Debug.Log("URL: " + url);
					}

					if (www.error != null) {
						UnityEngine.Debug.LogWarning("WWW Error: "+ www.error);
					} 
					else {
						if (isEnableLog()) {
							UnityEngine.Debug.Log("WWW Ok!: " + www.text);
						}
					}

					if (response != null) response(www);
					www.Dispose();
				}

				IEnumerator connect(string url, System.Action<WWW> response = null) {
					var www = new WWW(url);
					yield return www;

					if (www.error != null) {
						UnityEngine.Debug.Log("WWW Error: "+ www.error);
					} 
					else if (isEnableLog()) {
						UnityEngine.Debug.Log("WWW Ok!: " + www.text);
					}

					if (response != null) response(www);
					www.Dispose();
				}

			}

			public static Sprite loadSpriteFromWWW(WWW from) {
				using (from) {
					return Sprite.Create(
						from.texture, 
						new Rect(0,0,from.texture.width, from.texture.height), 
						new Vector2(0.5f,0.5f)
					);
				}		
			}

		}

	}
}