using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SLib.IO;

namespace SLib {
	namespace IO {

		public class TextRes : MonoBehaviour {

			private static Dictionary<string, string> strings = new Dictionary<string, string>();

			public static void loadStrings() {
				loadStrings("strings");
			}

			public static void loadStrings(string res_name) {
				string last_key = null;

				var csv = new CSVParser();
				csv.setOnReadColumn((int row_idx, int col_idx, string text) => {
					switch (col_idx) {
					case 0:
						last_key = text.Trim();;

						if (!string.IsNullOrEmpty(last_key)) {
							strings.Add(last_key, "");
						}
						break;

					case 1:
						if (!string.IsNullOrEmpty(last_key)) {
							strings[last_key] = text.Replace("\\n","\n").Trim();
						}
						break;
					}

					return true;
				});

				csv.skip_lines = 1;

				var src = Resources.Load<TextAsset>(res_name);
				csv.parseFromTextAsset(src);
			}

			public static string Get(string text_id) {
				if (strings.ContainsKey(text_id)) {
					return strings[text_id];
				}


				UnityEngine.Debug.LogWarning("not found string: " + text_id);
				return null;
			}

			public static string Format(string text_id, params object[] args) {
				return string.Format(Get(text_id), args);
			}


			public string text_id;

			void Start() {
				var text = Get(text_id);
				if (!string.IsNullOrEmpty(text)) {
					GetComponent<Text>().text = text;
				}
			}

		}

	}
}