﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;
using Ionic.Zip;
using System.Text;
using System.IO;


namespace SLib {
	namespace Platform {

		public class ZipUtil
		{
		#if UNITY_IOS
			[DllImport("__Internal")]
			private static extern void unzip (string zipFilePath, string location);
			[DllImport("__Internal")]
			private static extern void unzip_with_pass (string zipFilePath, string location, string pass);

		//	[DllImport("__Internal")]
		//	private static extern void zip (string zipFilePath);

		//	[DllImport("__Internal")]
		//	private static extern void addZipFile (string addFile);

		#endif

			public static void Unzip (string zipFilePath, string location, string password = null)
			{
		#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_STANDALONE_LINUX
				Directory.CreateDirectory (location);

				var options = new ReadOptions();

				#if UNITY_STANDALONE
				options.Encoding = Encoding.UTF8;
				#endif

				using (ZipFile zip = ZipFile.Read(zipFilePath, options)) {
					if (!string.IsNullOrEmpty(password)) {
						zip.Password = password;
					}

					#if UNITY_STANDALONE
					zip.AlternateEncoding = Encoding.UTF8;
					#endif
					zip.ExtractAll(location, ExtractExistingFileAction.OverwriteSilently);
				}

		#elif UNITY_ANDROID
				using (AndroidJavaClass zipper = new AndroidJavaClass(SLib.Config.Android.PLUGIN_UTIL_CLASS)) {
					if (string.IsNullOrEmpty(password)) {
						zipper.CallStatic ("unzip", zipFilePath, location);
					}
					else {
						zipper.CallStatic ("unzip_password", zipFilePath, location, password);
					}
				}
		#elif UNITY_IOS
				if (string.IsNullOrEmpty(password)) {
					unzip (zipFilePath, location);
				}
				else {
					unzip_with_pass (zipFilePath, location, password);
				}
		#endif
			}

			public static void Zip (string zipFileName, params string[] files)
			{
		#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_STANDALONE_LINUX
				string path = Path.GetDirectoryName(zipFileName);
				Directory.CreateDirectory (path);
				
				using (ZipFile zip = new ZipFile()) {
					foreach (string file in files) {
						zip.AddFile(file, "");
					}
					zip.Save (zipFileName);
				}
		#elif UNITY_ANDROID
				using (AndroidJavaClass zipper = new AndroidJavaClass(SLib.Config.Android.PLUGIN_UTIL_CLASS)) {
					{
						zipper.CallStatic ("zip", zipFileName, files);
					}
				}
		#elif UNITY_IOS
				foreach (string file in files) {
					addZipFile (file);
				}
				zip (zipFileName);
		#endif
			}
		}

	}
}