//
//  UnityZipFile.m
//  Unity-iPhone
//
//  Created by 山村 達彦 on 2013/12/29.
//
//
#import "ZipArchive.h"

static NSMutableArray* list = nil;

extern "C"{
    
   /* 
    void zip(const char*file) {
        NSString *zipPath =[NSString stringWithUTF8String:file];
        
        ZipArchive* zip = [[ZipArchive alloc] init];
        
        
        [zip CreateZipFile2:zipPath];
        
        for(int i=0; i<list.count; i++)
        {
            
            NSString* filePath = [list objectAtIndex:i];
            NSString* fileName = [filePath lastPathComponent];
            [zip addFileToZip:filePath newname:fileName];
        }
        
        [zip CloseZipFile2];
        [zip release];
        
        [list removeAllObjects];
        [list release];
        list = nil;
    }
    
    void addZipFile(const char*file)
    {
        NSString *zipPath =[NSString stringWithUTF8String:file];
        
        if( list == nil){
            list = [[NSMutableArray alloc] init];
        }
        [list addObject: zipPath];
    }
    */
	
	void unzip(char*file,  char* location) {
		NSString *zipPath =[NSString stringWithUTF8String:file];
		NSString *outputPath = [NSString stringWithUTF8String:location];
		
		[SSZipArchive unzipFileAtPath:zipPath toDestination:outputPath];
	}
	
    void unzip_with_pass( char*file,  char* location, char* pass)
    {
        NSString *zipPath =[NSString stringWithUTF8String:file];
        NSString *outputPath = [NSString stringWithUTF8String:location];
        NSString *password = [NSString stringWithUTF8String:pass];

        NSError *error = nil;
        [SSZipArchive unzipFileAtPath:zipPath toDestination:outputPath overwrite:YES password:password error:&error];
    }
}