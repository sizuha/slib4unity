﻿using UnityEngine;
using System.Collections;

namespace SLib {
	namespace Platform {

		public class Android : MonoBehaviour {

			public const string UNITY_PLAYER_CLASS = "com.unity3d.player.UnityPlayer";

			private static Android __instance;
			public static Android instance {
				get {
					#if !UNITY_ANDROID
					return null;
					#endif

					if (!__instance) {
						__instance = GameObject.FindObjectOfType<Android>();
						if (!__instance) {
							var newObj = new GameObject("AndroidManager");
							__instance = newObj.AddComponent<Android>();
						}

						DontDestroyOnLoad(__instance);
					}

					return __instance;
				}
			}

			private AndroidJavaClass unityClass = null;
			private AndroidJavaObject context = null;

			public AndroidJavaClass getUnityClass() {
				#if UNITY_ANDROID
				if (unityClass == null) {
					unityClass = new AndroidJavaClass(UNITY_PLAYER_CLASS);
					context = null;
				}

				return unityClass;
				#else
				return null;
				#endif
			}

			public AndroidJavaObject getUnityActivity() {
				#if UNITY_ANDROID
				return getUnityClass().GetStatic<AndroidJavaObject>("currentActivity");
				#else
				return null;
				#endif
			}

			public AndroidJavaObject getAppContext() {
				#if UNITY_ANDROID
				if (context == null) {
					context = getUnityActivity().Call<AndroidJavaObject>("getApplicationContext");
				}

				return context;
				#else
				return null;
				#endif
			}

			protected AndroidJavaClass getPluginClass() {
				return new AndroidJavaClass(Config.Android.PLUGIN_UTIL_CLASS);
			}

			public void release() {
				#if UNITY_ANDROID
				try {
					context.Dispose();
					unityClass.Dispose();
				}
				catch (System.Exception e) {
					Debug.Log(e);
				}
				#endif

				context = null;
				unityClass = null;
			}


			void Awake() {
				#if !UNITY_ANDROID
				Destroy(this);
				return;
				#endif

				if (!__instance) {
					__instance = this;
					DontDestroyOnLoad(__instance);
				}
			}

			void OnDestroy() {
				release();
			}


			//--- utils ------------

			public bool enableKeepScreenOn = false;

			void Start() {
				if (Application.platform == RuntimePlatform.Android) {
					if (enableKeepScreenOn) {
						setKeepScreenOn();
					}
				}
			}

			public long getFreeSpace() {
				using (var plugin = getPluginClass()) {
					return plugin.CallStatic<long>("getFreeSpace");
				}
			}

			public void setKeepScreenOn() {
				using (var plugin = getPluginClass()) {
					plugin.CallStatic("setKeepScreenOn");
				}				
			}

			public bool isSilentMode() {
				using (var plugin = getPluginClass()) {
					return plugin.CallStatic<bool>("isSilentMode");
				}				
			}

			public void sendSms(string sms_body) {
				using (var plugin = getPluginClass()) {
					plugin.CallStatic("sendSms", sms_body);
				}
			}

			public void sendEmail(string title, string subject, string message) {
				using (var plugin = getPluginClass()) {
					plugin.CallStatic("sendEmail", title, subject, message);
				}
			}

			public void openIntentByUrl(string url) {
				using (var plugin = getPluginClass()) {
					plugin.CallStatic("openIntentByUrl", url);
				}
			}

			public void showInfoDialog(int id, string receiverObjName, string title, string message, string ok_text) {
				using (var plugin = getPluginClass()) {
					plugin.CallStatic("showInfoDialog", id, title, message, receiverObjName, ok_text);
				}
			}

			public void showConfirmDialog(int id, string receiverObjName, string title, string message, string ok_text, string cancel_text) {
				using (var plugin = getPluginClass()) {
					plugin.CallStatic("showConfirmDialog", id, title, message, receiverObjName, ok_text, cancel_text);
				}
			}
		}

	}
}