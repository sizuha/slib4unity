#import <Foundation/Foundation.h>
//#import <MessageUI/MessageUI.h>

@class UtilsForUnity;
UtilsForUnity* util = nil;


UIViewController* getRootViewCtrl()
{
    id<UIApplicationDelegate> appDelegate = [[UIApplication sharedApplication] delegate];
    return appDelegate.window.rootViewController;
}

UIView* getRootView()
{
    return getRootViewCtrl().view;
}

void hideRootView()
{
    UIViewController* view_ctrl = getRootViewCtrl();
    [view_ctrl dismissViewControllerAnimated:YES completion:nil];
}

//--------------------------------------------------------

@interface UtilsForUnity: NSObject <UIAlertViewDelegate>
{
    NSString* unityObj;
    int dlg_id;
}

- (void)alertWithID:(int)alert_id title:(NSString*)title message:(NSString*)message textOk:(NSString*)ok_text unityObj:(NSString*)ns_unityObj;

/*- (BOOL)sendSms:(NSString*)message;
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result;

- (void)sendEmailSubject:(NSString*)subject message:(NSString*)message;
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error;*/

@end

@implementation UtilsForUnity
/*
- (BOOL)sendSms:(NSString*)message
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    
    if ([MFMessageComposeViewController canSendText]) {
        controller.body = message;
        //controller.recipients = [NSArray arrayWithObjects:@"12345678", @"87654321", nil];
        controller.messageComposeDelegate = self;
    }
    else {
        return NO;
    }
    
    // Present the view controller modally.
    UIViewController* view_ctrl = getRootViewCtrl();
    [view_ctrl presentViewController:controller animated:YES completion:nil];
    
    return YES;
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    // Dismiss the mail compose view controller.
    hideRootView();
}

- (void)sendEmailSubject:(NSString*)subject message:(NSString*)message
{
    MFMailComposeViewController* mailComposer = [[MFMailComposeViewController alloc] init];
    
    mailComposer.mailComposeDelegate = self;
    [mailComposer setSubject:subject];
    [mailComposer setMessageBody:message isHTML:YES];
    
    UIViewController* view_ctrl = getRootViewCtrl();
    [view_ctrl presentViewController:mailComposer animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    // Dismiss the mail compose view controller.
    hideRootView();
}
*/

- (void)alertWithID:(int)alert_id title:(NSString*)title message:(NSString*)message textOk:(NSString*)ok_text unityObj:(NSString*)ns_unityObj
{
    unityObj = ns_unityObj;
    dlg_id = alert_id;

    UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:ok_text otherButtonTitles: nil];

    alert.tag = 0;
    [alert show];
}

// アラートのボタンが押された時に呼ばれるデリゲート例文
-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 0) {
        UnitySendMessage([unityObj UTF8String], "onNativeDialogEvent", [[NSString stringWithFormat:@"ok,%d", dlg_id] UTF8String]) ;
    }
    else {
        switch (buttonIndex) {
        case 0:
          //１番目のボタンが押されたときの処理を記述する

          break;
        case 1:
          //２番目のボタンが押されたときの処理を記述する

          break;
        }
    }
}

@end

//--- UNITY Interface -----------------------------------------------------

void ios_init()
{
    if (!util) util = [[UtilsForUnity alloc] init];
}

void ios_release()
{
    util = nil;
}

void ios_setKeepScreenOn()
{
	NSLog(@"called: ios_setKeepScreenOn");
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}

BOOL ios_sendSms(char* message)
{
//    return [util sendSms:[NSString stringWithUTF8String:message]];
	return FALSE;
}

void ios_sendEmail(char* subject, char* message)
{
//    [util sendEmailSubject:[NSString stringWithUTF8String:subject] message:[NSString stringWithUTF8String:message]];
}

// Info -> Custom iOS Target Properties -> View controller-based status bar appearance = NO
void ios_hideStatusBar()
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
}

void ios_showStatusBar() 
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}

void ios_alert(const char* title, const char* message, const char* ok_btn_text) 
{
    NSString* ns_title = [NSString stringWithUTF8String: title];
    NSString* ns_msg = [NSString stringWithUTF8String: message];
    NSString* ns_ok_text = nil;

    if (ok_btn_text == NULL) {
        ns_ok_text = @"Ok";
    }
    else {
        ns_ok_text = [NSString stringWithUTF8String: ok_btn_text];   
    }

    UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle:ns_title message:ns_msg delegate:nil cancelButtonTitle:ns_ok_text otherButtonTitles: nil];
    [alert show];
}

void ios_alert_with_delegate(int dlg_id, const char* unity_obj, const char* title, const char* message, const char* ok_btn_text) 
{
    ios_init();

    NSString* ns_unityObj = [NSString stringWithUTF8String: unity_obj];
    NSString* ns_title = [NSString stringWithUTF8String: title];
    NSString* ns_msg = [NSString stringWithUTF8String: message];
    NSString* ns_ok_text = nil;

    if (ok_btn_text == NULL) {
        ns_ok_text = @"Ok";
    }
    else {
        ns_ok_text = [NSString stringWithUTF8String: ok_btn_text];   
    }

    [util alertWithID:dlg_id title:ns_title message:ns_msg textOk:ns_ok_text unityObj:ns_unityObj];
}
