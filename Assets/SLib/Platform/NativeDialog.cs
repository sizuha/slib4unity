﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SLib {
	namespace Platform {
		
		public class NativeDialog : MonoBehaviour {

			private static NativeDialog __instance;
			public static NativeDialog instance {
				get {
					if (!__instance) {
						__instance = GameObject.FindObjectOfType<NativeDialog>();
						if (!__instance) {
							var newObj = new GameObject("NativeDialogManager");
							__instance = newObj.AddComponent<NativeDialog>();
						}

						__instance.init();

						DontDestroyOnLoad(__instance);
					}

					return __instance;
				}
			}

			class ButtonMessageHandler {
				public System.Action onOk;
				public System.Action onCancel;
			}

			Dictionary<int,ButtonMessageHandler> messageMap;
			int last_dlg_id;

			protected void init() {
				messageMap = new Dictionary<int,ButtonMessageHandler>();
				last_dlg_id = -1;
			}

			public void alert(string title, string message, System.Action onOk = null) {
				alert(title, message, null, onOk);
			}

			public void alert(string title, string message, string ok_btn_text, System.Action onOk = null) {
				++last_dlg_id;

				if (onOk != null) {
					var handler = new ButtonMessageHandler();
					handler.onOk = onOk;
					messageMap.Add(last_dlg_id, handler);
				}

				#if UNITY_ANDROID
				Android.instance.showInfoDialog(last_dlg_id, gameObject.name, title, message, ok_btn_text); 
				#elif UNITY_IOS
				IOS.ios_alert_with_delegate(last_dlg_id, gameObject.name, title, message, ok_btn_text);
				#endif
			}

/*
			public void confirm(string message, System.Action onOk = null, System.Action onCancel = null) {
				confirm(message, null, null, onOk, onCancel);
			}

			public void confirm(string message, string ok_btn_text, string cancel_btn_text, System.Action onOk = null, System.Action onCancel = null) {
				++last_dlg_id;
			}
*/

			void onNativeDialogEvent(string param_csv) {
				var param_list = param_csv.Split(',');
				var key = int.Parse(param_list[1]);
				System.Action handler = null;

				if (param_list[0].StartsWith("ok")) {
					if (messageMap.ContainsKey(key)) {
						handler = messageMap[key].onOk;
					}
				}
				else {
					if (messageMap.ContainsKey(key)) {
						handler = messageMap[key].onCancel;
					}					
				}

				if (handler != null) handler();
				messageMap.Remove(key);
			}

		}


	}
}