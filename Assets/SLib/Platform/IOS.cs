﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;

namespace SLib {
	namespace Platform {

		public class IOS : MonoBehaviour {

			private static IOS __instance;
			public static IOS instance {
				get {
					#if !UNITY_IOS
					return null;
					#endif

					if (!__instance) {
						__instance = GameObject.FindObjectOfType<IOS>();
						if (!__instance) {
							var newObj = new GameObject("iOSManager");
							__instance = newObj.AddComponent<IOS>();
						}

						DontDestroyOnLoad(__instance);
					}

					return __instance;
				}
			}


			//--- import -----------------------------------

			#if UNITY_IOS && !UNITY_EDITOR
			[DllImport("__Internal")]
			protected static extern void ios_init();
			#else
			protected static void ios_init() {}
			#endif

			#if UNITY_IOS && !UNITY_EDITOR
			[DllImport("__Internal")]
			protected static extern void ios_release();
			#else
			protected static void ios_release() {}
			#endif

			#if UNITY_IOS && !UNITY_EDITOR
			[DllImport("__Internal")]
			protected static extern void ios_setKeepScreenOn();
			#else
			protected static void ios_setKeepScreenOn() {}
			#endif

			#if UNITY_IOS && !UNITY_EDITOR
			[DllImport("__Internal")]
			protected static extern bool ios_sendSms(string message);
			#else
			protected static bool ios_sendSms(string message) { return false; }
			#endif

			#if UNITY_IOS && !UNITY_EDITOR
			[DllImport("__Internal")]
			protected static extern void ios_sendEmail(string subject, string message);
			#else
			protected static void ios_sendEmail(string subject, string message) {}
			#endif

			#if UNITY_IOS && !UNITY_EDITOR
			[DllImport("__Internal")]
			protected static extern void ios_hideStatusBar();
			#else
			protected static void ios_hideStatusBar() {}
			#endif

			#if UNITY_IOS && !UNITY_EDITOR
			[DllImport("__Internal")]
			protected static extern void ios_showStatusBar();
			#else
			protected static void ios_showStatusBar() {}
			#endif

			//--- Alert Dlg ----

			#if UNITY_IOS && !UNITY_EDITOR
			[DllImport("__Internal")]
			protected static extern void ios_alert(string title, string message, string ok_text);
			#else
			protected static void ios_alert(string title, string message, string ok_text) {}
			#endif

			#if UNITY_IOS && !UNITY_EDITOR
			[DllImport("__Internal")]
			public static extern void ios_alert_with_delegate(int id, string unity_obj, string title, string message, string ok_text);
			#else
			public static void ios_alert_with_delegate(int id, string unity_obj, string title, string message, string ok_text) {}
			#endif

			//--------------------------------------


			public bool enableKeepScreenOn = false;

			void Awake() {
				#if !UNITY_IOS
				Destroy(this);
				return;
				#endif

				if (!__instance) {
					__instance = this;
					DontDestroyOnLoad(__instance);
				}

				ios_init();
			}

			void Start() {
				if (enableKeepScreenOn) {
					ios_setKeepScreenOn();
				}
			}

			void OnDestroy() {
				ios_release();
			}

			public void setKeepScreenOn() { ios_setKeepScreenOn(); }

			public bool sendSms(string message) {
				return ios_sendSms(message);
			}

			public void sendEmail(string subject, string message) {
				ios_sendEmail(subject, message);
			}

			public void setStatusBarVisible(bool show) {
				if (show)
					ios_showStatusBar();
				else
					ios_hideStatusBar();
			}

			public void alert(string title, string message, string ok_text) {
				ios_alert(title, message, ok_text);
			}

		}

	}
}