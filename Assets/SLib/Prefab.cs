﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace SLib {

	public class Prefab {

		public static GameObject loadToUIRoot(string prefab_res, bool is_active = true) {
			var prefab = Resources.Load<GameObject>(prefab_res);
			if (is_active) prefab.SetActive(true);
			return loadToUIRoot(prefab);
		}

		public static GameObject loadToUIRoot(string prefab_res, Vector2 localPosition, bool is_active = true) {
			var prefab = Resources.Load<GameObject>(prefab_res);
			if (is_active) prefab.SetActive(true);
			return loadToUIRoot(prefab, localPosition);
		}

		public static GameObject loadToUIRoot(GameObject prefab, Vector2 localPosition) {
			var obj = loadToUIRoot(prefab);
			obj.transform.localPosition = localPosition;
			return obj;
		}

		public static GameObject loadToUIRoot(GameObject prefab) {
			if (!prefab) {
				return null;
			}

			var obj = GameObject.Instantiate(prefab) as GameObject;
			obj.transform.SetParent(App.instance.getUIRoot());
			obj.transform.localScale = new Vector2(1,1);
			return obj;
		}

		public static GameObject load(string prefab_res, Transform parent, Vector2 localPosition, bool is_active = true) {
			var obj = load(prefab_res, parent);
			obj.transform.localPosition = localPosition;
			if (is_active) obj.SetActive(true);
			return obj;
		}

		public static GameObject load(string prefab_res, Transform parent, bool is_active = true) {
			var prefab = Resources.Load<GameObject>(prefab_res);
			var obj = GameObject.Instantiate(prefab) as GameObject;
			obj.transform.SetParent(parent);
			obj.transform.localScale = new Vector2(1,1);
			if (is_active) obj.SetActive(true);
			return obj;
		}

	}

}