﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SLib {
	namespace UI {

		public class Util {

			private static List<GameObject> fadeObj_list = new List<GameObject>();

			public static GameObject fadeout(byte alpha = 100) {
				var fadeObj = new GameObject();
				var img = fadeObj.AddComponent<Image>();
				img.color = new Color32(0, 0, 0, alpha);
				img.transform.SetParent(App.instance.getUIRoot());
				img.transform.localScale = new Vector2(Screen.width, Screen.height);

				fadeObj_list.Insert(0, fadeObj);	
				return fadeObj;
			}

			public static void fadein() {
				if (fadeObj_list.Count > 0) {
					var obj = fadeObj_list[0];
					fadeObj_list.RemoveAt(0);
					GameObject.Destroy(obj);
				}
			}

			public static void fadein_all() {
				foreach (var obj in fadeObj_list) {
					GameObject.Destroy(obj);
				}

				fadeObj_list.Clear();
			}

			public static bool is_fadeout() {
				return fadeObj_list.Count > 0;
			}

		}

	}	
}
