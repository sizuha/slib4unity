﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;

namespace SLib {
	namespace UI {

		[RequireComponent(typeof(Image))]
		public class SpriteLoader : MonoBehaviour {

			Image img;
			public string resourcePath;
			public bool loadFromResources = true;

			void Awake() {
				img = GetComponent<Image>();

				App.clearUnused();

				if (loadFromResources) {
					img.sprite = Resources.Load<Sprite>(resourcePath);
				}
				else {
					img.sprite = loadOrFindSprite(Application.persistentDataPath + "/" + resourcePath);
				}

				img.color = Color.white;
			}

			void OnDestroy() {
				App.clearUnused();
			}

			public static Sprite loadOrFindSprite(string filepath) {
				Texture2D tex = null;

				var file_info = new FileInfo(filepath);
				if (file_info.Exists) {
					var fileData = File.ReadAllBytes(filepath);
					tex = new Texture2D(2, 2);
					tex.LoadImage(fileData);

					return Sprite.Create(tex, new Rect(0,0,tex.width,tex.height), new Vector2(0.5f,0.5f));
				}

				return null;
			}
		}

	}
}