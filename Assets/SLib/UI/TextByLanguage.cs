﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


namespace SLib {
	namespace UI {

		[RequireComponent(typeof(Text))]
		public class TextByLanguage : MonoBehaviour {

			public string lang_default = "";
			public string lang_kor = "";
			public string lang_jp = "";

			void Start () {
				var text = GetComponent<Text>();

				string msg = null;

				if (Application.systemLanguage == SystemLanguage.Korean) {
					if (!string.IsNullOrEmpty(lang_kor)) msg = lang_kor;
				}
				else if (Application.systemLanguage == SystemLanguage.Japanese) {
					if (!string.IsNullOrEmpty(lang_jp)) msg = lang_jp;
				}
				else {
					if (!string.IsNullOrEmpty(lang_default)) msg = lang_default;
				}


				if (!string.IsNullOrEmpty(msg)) {
					text.text = msg.Replace("\\n", "\n");
				}
			}
			
		}

	}
}