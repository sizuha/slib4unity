﻿using UnityEngine;
using System.Collections;

namespace SLib {

	public class TweenPos : MonoBehaviour {

		public Vector2 moveTo;

		public float startDelay = 0;
		public float time;
		public bool global_move = false;
		#if USING_LEAN_TWEEN
		public LeanTweenType loopType = LeanTweenType.once;
		#endif

		Vector2 startPos;

		// Use this for initialization
		void Start () {
			if (enabled) {
				if (startDelay > 0) {
					StartCoroutine(startTweenAfter(startDelay));
				}
				else {
					startTween();
				}
			}
		}

		public IEnumerator startTweenAfter(float start_delay) {
			yield return new WaitForSeconds(start_delay);

			startTween();
		}

		public void startTween() {
			startTween(null);
		}

		public void startTween(System.Action onTweeenComplete) {
			#if USING_LEAN_TWEEN
			LTDescr options;

			if (global_move) {
				startPos = transform.position;
				options = LeanTween.move(gameObject, moveTo, time);
			}
			else {
				startPos = transform.localPosition;
				options = LeanTween.moveLocal(gameObject, moveTo, time);
			}

			options.setOnComplete(() => { if (onTweeenComplete != null) onTweeenComplete(); });
			options.setLoopType(loopType);
			#endif
		}

		public void returnTween() {
			returnTween(null);
		}

		public void returnTween(System.Action onTweeenComplete) {
			#if USING_LEAN_TWEEN
			if (global_move) {
				LeanTween.move(gameObject, startPos, time)
					.setOnComplete(() => { if (onTweeenComplete != null) onTweeenComplete(); });
			}
			else {
				LeanTween.moveLocal(gameObject, startPos, time)
					.setOnComplete(() => { if (onTweeenComplete != null) onTweeenComplete(); });
			}
			#endif
		}
		
	}

}