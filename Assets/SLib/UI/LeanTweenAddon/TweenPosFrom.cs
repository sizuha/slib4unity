﻿using UnityEngine;
using System.Collections;

namespace SLib {

	public class TweenPosFrom : MonoBehaviour {

		public Vector2 moveFrom;

		public float time;
		public bool global_move = false;

		Vector2 toPos;


		void Awake() {
			if (global_move) {
				toPos = transform.position;
				transform.position = moveFrom;
			}
			else {
				toPos = transform.localPosition;
				transform.localPosition = moveFrom;
			}
		}

		void Start() {
			startTween();
		}


		public void startTween(System.Action onTweeenComplete = null) {
			#if USING_LEAN_TWEEN
			if (global_move) {			
				LeanTween.move(gameObject, toPos, time)
					.setOnComplete(() => { if (onTweeenComplete != null) onTweeenComplete(); });
			}
			else {			
				LeanTween.moveLocal(gameObject, toPos, time)
					.setOnComplete(() => { if (onTweeenComplete != null) onTweeenComplete(); });
			}
			#endif
		}

		public void returnTween(System.Action onTweeenComplete = null) {
			#if USING_LEAN_TWEEN
			if (global_move) {
				LeanTween.move(gameObject, toPos, time)
					.setOnComplete(() => { if (onTweeenComplete != null) onTweeenComplete(); });
			}
			else {
				LeanTween.moveLocal(gameObject, toPos, time)
					.setOnComplete(() => { if (onTweeenComplete != null) onTweeenComplete(); });
			}
			#endif
		}

	}

}