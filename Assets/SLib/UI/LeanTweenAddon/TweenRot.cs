﻿using UnityEngine;
using System.Collections;

namespace SLib {
	
	public class TweenRot : MonoBehaviour {

		public Vector3 ratateTo;
		public float time;

		public bool pingpong = true;

		void Start() {
			if (enabled) {
				startTween();
			}
		}

		public void startTween() {
			#if USING_LEAN_TWEEN
			enabled = true;
			var tween = LeanTween.rotateLocal(gameObject, ratateTo, time);

			if (pingpong) {
				tween.setLoopPingPong();
			}
			#endif
		}

		public void stopTween(Vector3 returnRot) {
			#if USING_LEAN_TWEEN
			LeanTween.cancel(gameObject);
			LeanTween.rotateLocal(gameObject, returnRot, time);
			#endif
		}

	}

}