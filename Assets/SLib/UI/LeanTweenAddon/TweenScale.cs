﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace SLib {
	
	public class TweenScale : MonoBehaviour {

		public Vector3 scaleFrom;
		public Vector3 scaleTo;
		public float time;

		public UnityEvent onTweenComplete;
		public int loopCnt = 1;
		#if USING_LEAN_TWEEN
		public LeanTweenType loopType = LeanTweenType.once;
		#endif

		void Start() {
			if (enabled) {
				startTween();
			}
		}

		public void startTween() {
			#if USING_LEAN_TWEEN
			transform.localScale = scaleFrom;
			var tween = LeanTween.scale(gameObject, scaleTo, time)
				.setUseEstimatedTime(true)
				.setLoopType(loopType)
				.setOnComplete(() => { 
				if (onTweenComplete != null) {
					onTweenComplete.Invoke();
				}
			});

			if (loopCnt > 1) {
				tween.setLoopClamp().setLoopCount(loopCnt);
			}
			#endif
		}
		
	}

}