﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace SLib {

	public class TweenAlpha : MonoBehaviour {

		public bool textAlpha = false;

		public float tweenTime = 1f;

		#if USING_LEAN_TWEEN
		public LeanTweenType loopType = LeanTweenType.pingPong;
		#endif
		
		void Start () {
			var rectTransform = GetComponent<RectTransform>();
			tweenTo0(rectTransform);
		}
		
		void tweenTo0(RectTransform rectTransform) {
			#if USING_LEAN_TWEEN
			if (textAlpha) {
				LeanTween.textAlpha(rectTransform, 0, tweenTime)
					.setLoopType(loopType);
			}
			else {
				LeanTween.alpha(rectTransform, 0, tweenTime)
					.setLoopType(loopType);
			}
			#endif
		}

	}

}