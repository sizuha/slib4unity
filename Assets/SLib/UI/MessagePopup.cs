﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace SLib {
	namespace UI {

		[RequireComponent(typeof(PopupWindow))]
		public class MessagePopup : MonoBehaviour {

			public class Builder {

				private string prefab_name = "MessagePopup";
				private string message;
				private string ok_text, cancel_text;
				private System.Action onOk, onCancel;

				public Builder() {
					ok_text = null;
					cancel_text = null;
					onOk = null;
					onCancel = null;
				}

				public Builder setPrefab(string prefabName) {
					this.prefab_name = prefabName;
					return this;
				}

				public Builder setMessage(string message) {
					this.message =  message;
					return this;
				}

				public Builder setOkButton(string text, System.Action onClick = null) {
					ok_text = text;
					onOk = onClick;
					return this;
				}

				public Builder setOkButton(System.Action onClick) {
					ok_text = null;
					onOk = onClick;
					return this;
				}

				public Builder setCancelButton(string text, System.Action onClick = null) {
					cancel_text = text;
					onCancel = onClick;
					return this;
				}

				public Builder setCancelButton(System.Action onClick) {
					cancel_text = null;
					onCancel = onClick;
					return this;
				}


				public MessagePopup popup() {
					Util.fadeout();

					var obj = Prefab.loadToUIRoot(prefab_name, Vector2.zero);
					var popup = obj.GetComponent<MessagePopup>();

					popup.setPopup(message, ok_text, cancel_text, onOk, onCancel);
					return popup;
				}

			}


			public string groupName_Ok, groupName_OkCancel;
			public Button btnOk1, btnOk2, btnCancel;
			public Text txtMessage;

			void Start() {
				GetComponent<PopupWindow>().show();
			}

			public void setPopup(
				string message, 
				string okText, string cancelText,
				System.Action onOk, 
				System.Action onCancel) 
			{
				if (!string.IsNullOrEmpty(okText)) {
					if (btnOk1) btnOk1.GetComponentInChildren<Text>().text = okText;
					if (btnOk2) btnOk2.GetComponentInChildren<Text>().text = okText;
				}
				if (!string.IsNullOrEmpty(cancelText)) {
					btnCancel.GetComponentInChildren<Text>().text = cancelText;
				}

				if (onOk != null) {
					if (btnOk1) btnOk1.onClick.AddListener( () => onOk() );
					if (btnOk2) btnOk2.onClick.AddListener( () => onOk() );
				}

				if (onCancel != null) {
					if (btnCancel) btnCancel.onClick.AddListener( ()=> onCancel() );
				}

				var group1 = transform.FindChild(groupName_Ok);
				if (group1) {
					group1.gameObject.SetActive(cancelText == null && onCancel == null);
				}

				var group2 = transform.FindChild(groupName_OkCancel);
				if (group2) {
					group2.gameObject.SetActive(cancelText != null || onCancel != null);
				}

				if (!string.IsNullOrEmpty(message)) {
					txtMessage.text = message;

					var lines = message.Split('\n');
					if (lines.Length <= 2) {
						txtMessage.fontSize = (int)((float)txtMessage.fontSize*1.5f);
					}
					else if (lines.Length <= 4 && message.Length < 100) {
						txtMessage.fontSize = (int)((float)txtMessage.fontSize*1.25f);
					}
				}
			}
		}

	}
}