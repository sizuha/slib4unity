﻿using UnityEngine;
using System.Collections;

namespace SLib {
	namespace UI {

		public class ScrollToTop : MonoBehaviour {

			RectTransform rect;

			void Start() {
				rect = GetComponent<RectTransform>();
			}

			void Update() {
				moveToTop();
				Destroy(this);
			}

			public void moveToTop() {
				rect.localPosition =
					new Vector2(transform.localPosition.x, -rect.rect.height/2f);
			}

		}

	}
}