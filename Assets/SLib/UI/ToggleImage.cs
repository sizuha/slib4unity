﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace SLib {
	namespace UI {

		[RequireComponent(typeof(Image))]
		public class ToggleImage : MonoBehaviour {

			public Sprite imgOn, imgOff;
			public bool is_on = false;

			Image img;

			void Awake() {
				img = GetComponent<Image>();
			}

			void Start() {
				refresh();
			}

			public void toggle() {
				is_on = !is_on;
				refresh();
			}

			public void refresh() {
				img.sprite = is_on ? imgOn : imgOff;
			}

		}

	}
}