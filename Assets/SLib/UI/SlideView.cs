﻿using UnityEngine;
using System.Collections;


namespace SLib {
	namespace UI {

		public class SlideView : MonoBehaviour {

			public enum HideOption {
				NONE,
				DESTROY,
				DEACTIVE,
			}

			public HideOption hideOption = HideOption.DEACTIVE;

			TweenPos tween;

			void Start() {
				loadTween();
			}

			void loadTween() {
				tween = GetComponent<TweenPos>();
			}


			public void show() {
				if (!tween) loadTween();

				tween.startTween();
			}

			public void hide() {
				if (hideOption != HideOption.NONE) {
					tween.returnTween(() => {
						if (hideOption == HideOption.DESTROY) {
							Destroy(gameObject);
						}
						else {
							gameObject.SetActive(false);
						}
					});
				}
			}


		}
	}

}