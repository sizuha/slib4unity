﻿using UnityEngine;
using System.Collections;

namespace SLib {
	namespace UI {

		public class UIFragmentContent : MonoBehaviour {

			public UIFragment fragment;
			protected UIFragment getFragment() { return fragment; }

			virtual public void onFragmentStart(UIFragment parent) {
				this.fragment = parent;
			}

			virtual public void onFragmentDestroy() {}

		}

	}
}