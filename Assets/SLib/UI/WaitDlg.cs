﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace SLib {
	namespace UI {

		[RequireComponent(typeof(PopupWindow))]
		public class WaitDlg : MonoBehaviour {

			private PopupWindow popupWin;
			private Text text;

			PopupWindow getPopupWin() {
				if (!popupWin) popupWin = GetComponent<PopupWindow>();
				return popupWin;
			}

			Text getMsgText() {
				if (!text) text = GetComponentInChildren<Text>();
				return text;
			}

			public void hide() {
				try {
					getPopupWin().hide();
				}
				catch (System.Exception e) {
					UnityEngine.Debug.LogWarning(e);

					Util.fadein_all();
					Destroy(gameObject);
				}
			}

			public void setText(string message) {
				getMsgText().text = message;
			}
			
			public void hideAfter(float seconds) {
				StartCoroutine(coroutine_hide_after(seconds));
			}

			private IEnumerator coroutine_hide_after(float seconds) {
				yield return new WaitForSeconds(seconds);
				hide();
			}		

			public static WaitDlg popup(string prefab, string message = null, float hide_after_sec = 0) {
				Util.fadeout();
				var dlgObj = Prefab.loadToUIRoot(prefab, Vector2.zero);
				var dlg = dlgObj.GetComponent<WaitDlg>();

				if (!string.IsNullOrEmpty(message)) {
					dlg.setText(message);
				}
				
				if (hide_after_sec > 0) {
					dlg.hideAfter(hide_after_sec);
				}

				return  dlg;
			}

			public static void hideAll() {
				var objs = FindObjectsOfType<WaitDlg>();
				foreach (var o in objs) {
					try { o.hide(); } catch (System.Exception e) {
						UnityEngine.Debug.LogWarning(e);
					}
				}
			}

		}
	
	}
}