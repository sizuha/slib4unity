﻿using UnityEngine;
using System.Collections;


namespace SLib {
	namespace UI {
	
		public class UIFragment : MonoBehaviour {

			private UIFragmentContent currentContent;

			public string startWithPrefab;

			void Awake() {
				if (!string.IsNullOrEmpty(startWithPrefab)) {
					changeTo(startWithPrefab);
				}
			}

			public GameObject changeTo(string prefab) {
				return changeTo(prefab, Vector2.zero);
			}

			public GameObject changeTo(string prefab, Vector2 localPosition) {
				var created = Prefab.load(prefab, transform, localPosition);
				created.SetActive(true);

				var content = created.GetComponent<UIFragmentContent>();
				if (content) {
					changeContent(content);
				}
				else {
					releaseCurrentContent();
					currentContent = null;
				}

				return created;
			}

			public void changeTo(UIFragmentContent content) {
				content.gameObject.SetActive(true);
				changeTo(content, Vector2.zero);
			}

			public void changeTo(UIFragmentContent content, Vector2 localPostion) {
				content.transform.SetParent(transform);
				content.transform.localPosition = localPostion;
				content.transform.localScale = Vector2.one;
				changeContent(content);
			}

			void changeContent(UIFragmentContent content) {
				releaseCurrentContent();

				content.onFragmentStart(this);
				currentContent = content;

				App.clearUnused();
			}

			void releaseCurrentContent() {
				if (currentContent) {
					currentContent.onFragmentDestroy();
					Destroy(currentContent.gameObject);
				}
			}

		}

	}
}