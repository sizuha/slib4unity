﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace SLib {
	namespace UI {

		public class PopupWindow : MonoBehaviour {

			public bool setPositionToLocalZero = true;
			public bool enableStartTween = true;

			TweenScale tweenS;


			void Start() {
				show();
			}

			void addTween() {
				tweenS = gameObject.AddComponent<TweenScale>();
				tweenS.onTweenComplete = new UnityEvent();
				tweenS.time = 0.1f;
			}

			public void show() {
				if (setPositionToLocalZero) {
					transform.localPosition = Vector3.zero;
				}

				if (!tweenS) {
					addTween();
				}

				if (enableStartTween) {
					tweenS.scaleFrom = Vector3.zero;
					tweenS.scaleTo = Vector3.one;
					tweenS.startTween();
				}
				else {
					tweenS.scaleFrom = Vector3.one;
					tweenS.scaleTo = Vector3.one;
					transform.localScale = Vector3.one;
				}
			}

			public void hide() {
				tweenS.scaleFrom = Vector3.one;
				tweenS.scaleTo = Vector3.zero;

				tweenS.onTweenComplete.AddListener(
					()=>{
						Destroy(gameObject);
					}
				);
				tweenS.startTween();

				Util.fadein();
			}
			
		}

	}
}