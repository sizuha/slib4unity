﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SLib {
	
	public class Assert {

		public delegate void TestDelegate();

		public static void True(bool cond) {
			#if DEBUG || UNITY_EDITOR
			if (!cond) throw new System.Exception("Assert Failed.");
			#endif
		}

		public static void False(bool cond) {
			#if DEBUG || UNITY_EDITOR
			True(!cond);
			#endif
		}

		public static void Catch<E>(TestDelegate code) where E : System.Exception { 
			#if DEBUG || UNITY_EDITOR
			try { code(); } catch (E) { /* ok */  }
			#endif
		}

	}

}
