package plugins.unity.sizuha.util;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

import androidx.loader.content.CursorLoader;
import androidx.core.content.FileProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * ライブラリーの中で使う為のUtility Functions
 *
 */
public final class Utils {

    public static void openImageBrowser(Activity from, int request_code) {
        final Intent chooseIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        chooseIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
        chooseIntent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        from.startActivityForResult(chooseIntent, request_code);
    }

    public static void openImageCapture(Activity from, int request_code, String output) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (output != null && output.length() > 0) {
            final Uri uri = getContentUri(from, output);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        }

        if (takePictureIntent.resolveActivity(from.getPackageManager()) != null) {
            from.startActivityForResult(takePictureIntent, request_code);
        }
    }

    public static void openVideoCapture(Activity from, int request_code, String output) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        if (output != null && output.length() > 0) {
            final Uri uri = getContentUri(from, output);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        }

//        takePictureIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, );
//        takePictureIntent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, );
//        takePictureIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, );

        if (takePictureIntent.resolveActivity(from.getPackageManager()) != null) {
            from.startActivityForResult(takePictureIntent, request_code);
        }
    }

    public static String getFilepathFromMediaStore(Context context, Intent data) {
        final Uri uri = data.getData();

        final String[] filepath = { MediaStore.Images.Media.DATA };
        final Cursor cursor = context.getContentResolver().query(uri, filepath, null, null, null);
        if (cursor == null) return null;

        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filepath[0]);
        String file_path = cursor.getString(columnIndex);
        cursor.close();

        return file_path;
    }

    public static String getRealPathFromURI_API19(Context context, Uri uri) {
        if (android.os.Build.VERSION.SDK_INT < 19) return null;

        if ("com.android.providers.downloads.documents".equals(uri.getAuthority())) {
            // ダウンロードからの場合
            String id = DocumentsContract.getDocumentId(uri);
            Uri docUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

            final Cursor cursor = context.getContentResolver().query(docUri, new String[]{MediaStore.MediaColumns.DATA}, null, null, null);
            if (cursor == null) return null;

            if (cursor.moveToFirst()) {
                final File file = new File(cursor.getString(0));
                cursor.close();
                return file.getAbsolutePath();
            }
        }

        String filePath = "";
        String wholeID;
        String id;

        try {
            wholeID = DocumentsContract.getDocumentId(uri);

            final String[] result = wholeID.split(":");
            id = result[1];
        }
        catch (Exception e) {
            return null;
        }

        final String[] column = { MediaStore.Images.Media.DATA };

        // where user_idx is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{ id }, null
        );

        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }


    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        if (android.os.Build.VERSION.SDK_INT < 11 || android.os.Build.VERSION.SDK_INT > 18)
            return null;

        final String[] proj = { MediaStore.Images.Media.DATA };
        String result = null;

        final CursorLoader cursorLoader = new CursorLoader(context, contentUri, proj, null, null, null);
        final Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }


    public static void scaleBitmap(InputStream is, OutputStream os, float newWidth, float newHeight) {
        final Bitmap src_bitmap = BitmapFactory.decodeStream(is);
        final Bitmap scaled_bitmap = scaleBitmap(src_bitmap, newWidth, newHeight);
        src_bitmap.recycle();

        scaled_bitmap.compress(Bitmap.CompressFormat.JPEG, 90, os);
        scaled_bitmap.recycle();
    }

    public static Bitmap scaleBitmap(Bitmap bitmapToScale, float newWidth, float newHeight) {
        if(bitmapToScale == null)
            return null;
        //get the original width and height
        int width = bitmapToScale.getWidth();
        int height = bitmapToScale.getHeight();
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();

        // resize the bit map
        matrix.postScale(newWidth / width, newHeight / height);

        // recreate the new Bitmap and set it back
        return Bitmap.createBitmap(bitmapToScale, 0, 0, bitmapToScale.getWidth(), bitmapToScale.getHeight(), matrix, true);
    }

    public static Uri getContentUri(Context c, String path) {
        final File file = new File(path);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            return Uri.fromFile(file);
        }

        final String authority = c.getApplicationContext().getPackageName() + ".provider";
        return FileProvider.getUriForFile(c, authority, file);
    }


    // for JSON

    public static String getJsonString(JSONObject json, String key) {
        return getJsonString(json, key, "");
    }

    public static String getJsonString(JSONObject json, String key, String defaultVal) {
        try {
            if (json.has(key)) {
                return json.getString(key);
            }
        }
        catch (JSONException e) {}

        return defaultVal;
    }

    public static int getJsonInt(JSONObject json, String key) {
        return getJsonInt(json, key, 0);
    }

    public static int getJsonInt(JSONObject json, String key, int defaultVal) {
        try {
            if (json.has(key)) {
                return json.getInt(key);
            }
        }
        catch (JSONException e) {}

        return defaultVal;
    }



}
