package plugins.unity.sizuha.util;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;

import java.io.File;
import java.io.*;
import java.util.Calendar;
import java.util.List;
import java.util.zip.*;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import com.unity3d.player.UnityPlayer;

import static android.content.Context.MODE_PRIVATE;

/**
 * Unity側でコールする、API
 *
 */
public class UnityUtil {
    final static int BUFFER_SIZE = 512;
    final static String TAG = "unity";

    /**
     * AARにExportする時、最適化により、どこでも参照いていないメソッドがAARファイルに含まれない場合がある。
     * それを防止する為に、ここで参照を作る。
     */
    public static void dummy() {
        try {
            getFreeSpace();
            setKeepScreenOn();
            isSilentMode();
            sendEmail("", "", "");
            openIntentByUrl("");
            showInfoDialog(0, "","","","");
            showConfirmDialog(0, "", "", "", "", "");

            stopSpeechToText();
            startSpeechToText("", "", "", "");
            runSpeechToTextUI("", "", "");
            showBrowseImageUI("", "", "");
            showCaptureImageUI("", "", "");

            setEnableLocalNotification(1, new int[]{});

            getLocalNotificationSettings("", "");
            setEnableNotification(false);
            checkNotificationEnabled("", "");

            requestPermissions();
        }
        catch (Exception ignored) { }
    }

    /***
     * @deprecated C#のコードを使って下さい
     */
    public static void zip(String zipFile, String[] files) throws IOException {
        BufferedInputStream origin = null;
        final ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFile)));
        try {
            final byte data[] = new byte[BUFFER_SIZE];

            for (int i = 0; i < files.length; i++) {
                final FileInputStream fi = new FileInputStream(files[i]);
                origin = new BufferedInputStream(fi, BUFFER_SIZE);
                try {
                    final ZipEntry entry = new ZipEntry(files[i].substring(files[i].lastIndexOf("/") + 1));
                    out.putNextEntry(entry);
                    int count;
                    while ((count = origin.read(data, 0, BUFFER_SIZE)) != -1) {
                        out.write(data, 0, count);
                    }
                }
                finally {
                    origin.close();
                }
            }
        }
        finally {
            out.close();
        }
    }

    /***
     * @deprecated C#のコードを使って下さい
     */
    public static void unzip(String zipFile, String location) {
        int size;
        final byte[] buffer = new byte[BUFFER_SIZE];

        try {
            if ( !location.endsWith("/") ) {
                location += "/";
            }
            final File f = new File(location);
            if(!f.isDirectory()) {
                f.mkdirs();
            }
            final ZipInputStream zin = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile), BUFFER_SIZE));
            try {
                ZipEntry ze = null;
                while ((ze = zin.getNextEntry()) != null) {
                    final String path = location + ze.getName();
                    final File unzipFile = new File(path);

                    if (ze.isDirectory()) {
                        if(!unzipFile.isDirectory()) {
                            unzipFile.mkdirs();
                        }
                    } else {
                        // check for and create parent directories if they don't exist
                        final File parentDir = unzipFile.getParentFile();
                        if ( null != parentDir ) {
                            if ( !parentDir.isDirectory() ) {
                                parentDir.mkdirs();
                            }
                        }

                        // unzip the file
                        final FileOutputStream out = new FileOutputStream(unzipFile, false);
                        final BufferedOutputStream fout = new BufferedOutputStream(out, BUFFER_SIZE);
                        try {
                            while ( (size = zin.read(buffer, 0, BUFFER_SIZE)) != -1 ) {
                                fout.write(buffer, 0, size);
                            }

                            zin.closeEntry();
                        }
                        finally {
                            fout.flush();
                            fout.close();
                        }
                    }
                }
            }
            finally {
                zin.close();
            }
        }
        catch (Exception e) {
            Log.e(TAG, "Unzip exception", e);
        }
    }

    /***
     * @deprecated C#のコードを使って下さい
     */
	public static void unzip_password(String zipFile, String location, String password) {
		try {
			// Initiate ZipFile object with the path/name of the zip file.
            final ZipFile zip = new ZipFile(zipFile);
			zip.setPassword(password);

			// Extracts all files to the path specified
			zip.extractAll(location);

		} catch (ZipException e) {
			e.printStackTrace();
		}
	}


    public static long getFreeSpace() {
        final Activity c = UnityPlayer.currentActivity;
	    if (c == null) return -1;

        final File internalStorageFile = c.getFilesDir();
        return internalStorageFile.getFreeSpace();
    }

    public static void setKeepScreenOn() {
	    Log.i(TAG, "call: setKeepScreenOn");
        final Activity a = UnityPlayer.currentActivity;

	    a.runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
			    a.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		    }
	    });
    }

    public static boolean isSilentMode() {
        final Activity a = UnityPlayer.currentActivity;

	    try {
            final AudioManager am = (AudioManager)a.getSystemService(Context.AUDIO_SERVICE);
            switch (am.getRingerMode()) {
	            case AudioManager.RINGER_MODE_SILENT:
		            Log.i(TAG, "Silent mode");
		            return true;
	            case AudioManager.RINGER_MODE_VIBRATE:
		            Log.i(TAG, "Vibrate mode");
		            return true;
	            case AudioManager.RINGER_MODE_NORMAL:
		            Log.i(TAG, "Normal mode");
		            return false;
            }
        }
	    catch (Exception e) {
		    return false;
	    }

        return false;
    }

    public static void sendSms(String sms_body) {
        final Activity a = UnityPlayer.currentActivity;

        final Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.putExtra("sms_body", sms_body);
        sendIntent.setType("vnd.android-dir/mms-sms");
        a.startActivity(sendIntent);
    }

    public static void sendEmail(String title, String subject, String message) {
        final Activity a = UnityPlayer.currentActivity;
	    final Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setDataAndType(Uri.parse("mailto:"), "text/plain");
	    //emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
	    emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
	    emailIntent.putExtra(Intent.EXTRA_TEXT, message);

	    try {
		    a.startActivity(Intent.createChooser(emailIntent, title));
	    }
	    catch (android.content.ActivityNotFoundException ex) {
		    // nothing
	    }
    }

    public static void openIntentByUrl(String url) {
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addCategory(Intent.CATEGORY_BROWSABLE);
        intent.setData(Uri.parse(url)); // ex) "bill2://test2?r=received"
        UnityPlayer.currentActivity.startActivity(intent);
    }

    public static void showInfoDialog(final int dlg_id, final String title, final String message, final String receiverObjName, String ok_text)
    {
        final Activity c = UnityPlayer.currentActivity;
        final String str_ok = (ok_text == null || ok_text.length() < 1) ? c.getString(android.R.string.ok) : ok_text;

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final AlertDialog dlg = new AlertDialog.Builder(c)
                        .setCancelable(false)
                        .setTitle(title)
                        .setMessage(message)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setPositiveButton(str_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                UnityPlayer.UnitySendMessage(receiverObjName, "onNativeDialogEvent", String.format("ok,%d", dlg_id));
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                                if (keyCode == KeyEvent.KEYCODE_BACK) {
                                    UnityPlayer.UnitySendMessage(receiverObjName, "onNativeDialogEvent", String.format("ok,%d", dlg_id));
                                }
                                return false;
                            }
                        })
                        .create();

                dlg.setCanceledOnTouchOutside(false);
                dlg.show();
            }
        });
    }

    public static void showConfirmDialog(final int dlg_id, final String title, final String message, final String receiverObjName, String ok_text, String cancel_text)
    {
        final Activity c = UnityPlayer.currentActivity;
        final String str_ok = (ok_text == null || ok_text.length() < 1) ? c.getString(android.R.string.ok) : ok_text;
        final String str_cancel = (cancel_text == null || cancel_text.length() < 1) ? c.getString(android.R.string.cancel) : cancel_text;

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog dlg = new AlertDialog.Builder(c)
                        .setTitle(title)
                        .setMessage(message)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setPositiveButton(str_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                UnityPlayer.UnitySendMessage(receiverObjName, "onNativeDialogEvent", String.format("ok,%d", dlg_id));
                            }
                        })
                        .setNegativeButton(str_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                UnityPlayer.UnitySendMessage(receiverObjName, "onNativeDialogEvent", String.format("cancel,%d", dlg_id));
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                                if (keyCode == KeyEvent.KEYCODE_BACK) {
                                    UnityPlayer.UnitySendMessage(receiverObjName, "onNativeDialogEvent", String.format("cancel,%d", dlg_id));
                                }
                                return false;
                            }
                        })
                        .create();

                dlg.setCanceledOnTouchOutside(false);
                dlg.show();
            }
        });
    }

    static SpeechRecognizer m_recognizer;

    public static void stopSpeechToText() {
        final Activity a = UnityPlayer.currentActivity;
        a.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (m_recognizer != null) {
                    m_recognizer.stopListening();
                    m_recognizer = null;
                }
            }
        });
    }

    public static void cancelSpeechToText() {
        final Activity a = UnityPlayer.currentActivity;
        a.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (m_recognizer != null) {
                    m_recognizer.cancel();
                    m_recognizer = null;
                }
            }
        });
    }

    public static void startSpeechToText(final String recv_obj, final String response_func, final String update_func, final String err_func) {
        final Activity a = UnityPlayer.currentActivity;

        if (!SpeechRecognizer.isRecognitionAvailable(a)) {
            Log.i("recognizer", "onError=>Recognition is not available");
            UnityPlayer.UnitySendMessage(recv_obj, err_func, "NOT_AVAILABLE");
            return;
        }

        final Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, a.getPackageName());
        intent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
        //intent.putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, useOffline);

        a.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                cancelSpeechToText();

                m_recognizer = SpeechRecognizer.createSpeechRecognizer(a);
                m_recognizer.setRecognitionListener(new RecognitionListener() {
                    @Override
                    public void onReadyForSpeech(Bundle params) {
                        Log.i("recognizer", "onReadyForSpeech");
                    }

                    @Override
                    public void onBeginningOfSpeech() {
                        Log.i("recognizer", "onBeginningOfSpeech");
                    }

                    @Override
                    public void onRmsChanged(float rmsdB) {
                        //Log.i("recognizer", "onRmsChanged");
                    }

                    @Override
                    public void onBufferReceived(byte[] buffer) {
                        Log.i("recognizer", "onBufferReceived");
                    }

                    @Override
                    public void onEndOfSpeech() {
                        Log.i("recognizer", "onEndOfSpeech");
                    }

                    @Override
                    public void onError(int error) {
                        Log.i("recognizer", "onError=>"+error);
                        UnityPlayer.UnitySendMessage(recv_obj, err_func, String.valueOf(error));
                    }

                    @Override
                    public void onResults(Bundle results) {
                        final List<String> recData = results.getStringArrayList(android.speech.SpeechRecognizer.RESULTS_RECOGNITION);
                        if (recData != null) {
                            Log.i("recognizer", "onResults=>"+ recData.get(0));
                            UnityPlayer.UnitySendMessage(recv_obj, response_func, recData.get(0));
                        }
                        else {
                            UnityPlayer.UnitySendMessage(recv_obj, response_func, "");
                        }
                    }

                    @Override
                    public void onPartialResults(Bundle partialResults) {
                        final List<String> recData = partialResults.getStringArrayList(android.speech.SpeechRecognizer.RESULTS_RECOGNITION);
                        if (recData != null) {
                            Log.i("slrecognizerib", "onPartialResults=>"+recData.get(0));
                            UnityPlayer.UnitySendMessage(recv_obj, update_func, recData.get(0));
                        }
//                else {
//                    UnityPlayer.UnitySendMessage(recv_obj, update_func, "");
//                }
                    }

                    @Override
                    public void onEvent(int eventType, Bundle params) {
                        Log.i("recognizer", "onEvent=>"+eventType);
                    }
                });

                m_recognizer.startListening(intent);
            }
        });
    }

    public static void runSpeechToTextUI(String prompt, String recv_obj, String recv_func) {
        final Activity a = UnityPlayer.currentActivity;
        final Intent intent = new Intent(a, plugins.unity.sizuha.util.ApiActivity.class);
        intent.putExtra("api", ApiActivity.API_SPEECH_TO_TEXT);
        intent.putExtra("message", prompt);
        intent.putExtra("recv_obj", recv_obj);
        intent.putExtra("recv_func", recv_func);
        a.startActivity(intent);
    }

    public static void showBrowseImageUI(String prompt, String recv_obj, String recv_func) {
        final Activity a = UnityPlayer.currentActivity;
        final Intent intent = new Intent(a, plugins.unity.sizuha.util.ApiActivity.class);
        intent.putExtra("api", ApiActivity.API_BROWSE_IMG);
        intent.putExtra("message", prompt);
        intent.putExtra("recv_obj", recv_obj);
        intent.putExtra("recv_func", recv_func);
        a.startActivity(intent);
    }

    public static void showCaptureImageUI(String outpath, String recv_obj, String recv_func) {
        final Activity a = UnityPlayer.currentActivity;
        final Intent intent = new Intent(a, plugins.unity.sizuha.util.ApiActivity.class);
        intent.putExtra("api", ApiActivity.API_TAKE_PIC);
        intent.putExtra("recv_obj", recv_obj);
        intent.putExtra("recv_func", recv_func);
        intent.putExtra("param", outpath);
        a.startActivity(intent);
    }

    static SharedPreferences getSettings() {
        final Activity a = UnityPlayer.currentActivity;

        final SharedPreferences pref = a.getSharedPreferences("unity_app_pref", MODE_PRIVATE);
        return pref;
    }


    //--- Notification ---

    public static void getLocalNotificationSettings(String recv_obj, String recv_func) {
        final SharedPreferences pref = getSettings();

        // Default: Calendar.SUNDAY(1) ~ Calendar.SATURDAY(7)
        final String value = pref.getString("noti_day", "1,2,3,4,5,6,7");
        UnityPlayer.UnitySendMessage(recv_obj, recv_func, value);
    }

    public static void setEnableLocalNotification(int hour, int[] dayOfWeek) {
        String output = "";
        boolean isFirst = true;

        for (int dayVal: dayOfWeek) {
            if (isFirst) {
                isFirst = false;
            }
            else {
                output += ",";
            }

            output += String.valueOf(dayVal);
        }

        getSettings().edit().putString("noti_day", output).apply();

        final Activity a = UnityPlayer.currentActivity;
        final Intent intent = new Intent(a, AlarmReceiver.class);
        AlarmUtils.cancelAllAlarms(a, intent);

        final Calendar day = Calendar.getInstance();
        day.set(Calendar.HOUR_OF_DAY, hour);
        day.set(Calendar.MINUTE, 0);
        day.set(Calendar.SECOND, 0);
        setLocalAlarmEveryDays(day, 111);
    }

    public static void checkNotificationEnabled(String recv_obj, String recv_func) {
        final SharedPreferences pref = getSettings();
        final String value = pref.getBoolean("enable_noti", true) ? "1" : "0";
        UnityPlayer.UnitySendMessage(recv_obj, recv_func, value);
    }

    public static void setEnableNotification(boolean onOff) {
        getSettings().edit().putBoolean("enable_noti", onOff).apply();
    }

    static void setLocalAlarmEveryDays(Calendar startAt, int notiID) {
        final Activity a = UnityPlayer.currentActivity;
        final Intent notificationIntent = new Intent(a, ApiActivity.class);

        final long interval = AlarmManager.INTERVAL_DAY;
        AlarmUtils.addRepeatAlarm(a, notificationIntent, notiID, startAt, interval);
    }

    //--- Permissions ---

    public static void requestPermissions() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return;

        final Activity a = UnityPlayer.currentActivity;
        final int REQUEST_MICROPHONE = 627901;

        a.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return;

                if (a.checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
                {
                    Log.d("UnityUtil", "requestPermissions -> NOT GRANTED");
                    if (a.shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)) {
                        Log.d("UnityUtil", "shouldShowRequestPermissionRationale = true");
                        AlertDialog.Builder builder = new AlertDialog.Builder(a);
                        builder.setTitle("権限が必要です")
                                .setMessage("発音テストと録音の為に、マイクの使用許可が必要です。")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                            a.requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_MICROPHONE);
                                        }
                                    }
                                });
                        builder.create().show();
                    }
                    else {
                        a.requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_MICROPHONE);
                    }
                }
                else {
                    Log.d("UnityUtil", "requestPermissions -> PERMISSION_GRANTED");
                }
            }
        });
    }



/*
    //--- LINE SDK ---//
    public static void linesdk_init(Context app_context) {
        LineSdkContextManager.initialize(app_context);
    }

    private static AccessToken line_acc_tkn;

    public static void linesdk_login(Activity activity, final String listener_obj) {
        LineSdkContext sdkContext = LineSdkContextManager.getSdkContext();

        LineAuthManager authManager = sdkContext.getAuthManager();
        authManager.addOnAccessTokenChangedListener(new OnAccessTokenChangedListener() {
            @Override
            public void onAccessTokenChanged(final AccessToken newAccessToken) {
                line_acc_tkn = newAccessToken;
            }
        });

        LineLoginFuture loginFuture = authManager.login(activity);
        loginFuture.addFutureListener(new LineLoginFutureListener() {
            @Override
            public void loginComplete(LineLoginFuture future) {
                switch(future.getProgress()) {
                    case SUCCESS:
                        line_acc_tkn = future.getAccessToken();

                        UnityPlayer.UnitySendMessage(listener_obj,
                                "onLineSdkLoginEvent",
                                "SUCCESS,"+line_acc_tkn.accessToken+","+line_acc_tkn.refreshToken);
                        break;
                    case CANCELED: // Login canceled by user
                        UnityPlayer.UnitySendMessage(listener_obj, "onLineSdkLoginEvent", "CANCELED");
                        break;
                    default: // Error
                        UnityPlayer.UnitySendMessage(listener_obj, "onLineSdkLoginEvent", "ERROR");
                        break;
                }
            }
        });
    }

    public static void linesdk_logout() {
        LineSdkContext sdkContext = LineSdkContextManager.getSdkContext();
        LineAuthManager authManager = sdkContext.getAuthManager();
        authManager.logout();

        line_acc_tkn = null;
    }

    public static void linesdk_send_message(final String listener_obj) {
        LineSdkContext sdkContext = LineSdkContextManager.getSdkContext();
        ApiClient api = sdkContext.getApiClient();
        //api.postEvent()
    }
*/
    //------------------------------------------------------

}
