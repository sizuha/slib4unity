package plugins.unity.sizuha.util.fcm;


import org.json.JSONObject;

import plugins.unity.sizuha.util.Utils;

public final class PushMessage {

    public String message;

    public PushMessage() {}

    public PushMessage(JSONObject json) {
        this.message = Utils.getJsonString(json, "message", "");
    }

    public PushMessage(String message) {
        this.message = message;
    }

}
