package plugins.unity.sizuha.util;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.unity3d.player.UnityPlayer;

import java.util.ArrayList;

public class ApiActivity extends Activity {

    public final static int API_NONE = 0;
    public final static int API_SPEECH_TO_TEXT = 1000;
    public final static int API_BROWSE_IMG = 2000;
    public final static int API_TAKE_PIC = 2100;

    private int api_code = API_NONE;
    private String message;
    private String receiveObj, receiveFunc;
    private String param;

    public static String dummy() {
        return "";
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        final Intent intent = getIntent();

        final Bundle extras = intent.getExtras();
        if (extras != null) {
            api_code = extras.getInt("api", API_NONE);
            message = extras.getString("message", "");
            receiveObj = extras.getString("recv_obj");
            receiveFunc = extras.getString("recv_func");
            param = extras.getString("param", "");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        switch (api_code) {
            case API_SPEECH_TO_TEXT:
                startSpeechToText();
                break;

            case API_BROWSE_IMG:
                showImageChoiceUI();
                break;

            case API_TAKE_PIC:
                showTakePicUI();
                break;

            default:
                finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case API_SPEECH_TO_TEXT:
                if (resultCode == RESULT_OK && null != data) {
                    final ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    UnityPlayer.UnitySendMessage(receiveObj, receiveFunc, result.get(0));
                }
                else {
                    UnityPlayer.UnitySendMessage(receiveObj, receiveFunc, "");
                }
                break;

            case API_BROWSE_IMG: {
                final boolean isOk = resultCode == RESULT_OK && null != data;
                String file_path = null;

                if (isOk) {
                    final String[] projection = {MediaStore.Images.Media.DATA};
                    final ClipData selectedImages = data.getClipData();

                    if (selectedImages == null) {
                        final Cursor cur = getContentResolver().query(data.getData(), projection, null, null, null);
                        if (cur != null) {
                            if (cur.moveToFirst()) {
                                file_path = cur.getString(0);
                                Log.d("UnityUtil", "+ ImagePick(1): " + file_path);
                            }
                            cur.close();
                        }
                    }
                    else if (selectedImages.getItemCount() > 0) {
                        final ClipData.Item item = selectedImages.getItemAt(0);
                        final Cursor cur = getContentResolver().query(item.getUri(), projection, null, null, null);
                        if (cur != null) {
                            if (cur.moveToFirst()) {
                                file_path = cur.getString(0);
                                Log.d("UnityUtil", "+ ImagePick(2): " + file_path);
                            }
                            cur.close();
                        }
                    }
                }

                UnityPlayer.UnitySendMessage(receiveObj, receiveFunc, file_path == null ? "" : file_path);
                break;
            }

            case API_TAKE_PIC:
                final boolean isOk = resultCode == RESULT_OK;
                UnityPlayer.UnitySendMessage(receiveObj, receiveFunc, isOk ? "1" : "0");
                break;
        }

        finish();
    }

    private void requestRecordAudioPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String requiredPermission = Manifest.permission.RECORD_AUDIO;

            // If the user previously denied this permission then show a message explaining why
            // this permission is needed
            if (checkCallingOrSelfPermission(requiredPermission) == PackageManager.PERMISSION_DENIED) {
                requestPermissions(new String[]{requiredPermission}, 101);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 101:
                doStartSpeechToText();
                break;
        }
    }

    protected void startSpeechToText() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestRecordAudioPermission();
        }
        else {
            doStartSpeechToText();
        }
    }

    protected void doStartSpeechToText() {
        final Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, message);

        try {
            startActivityForResult(intent, API_SPEECH_TO_TEXT);
        }
        catch (ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(),
                    e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }

    protected void showImageChoiceUI() {
        Utils.openImageBrowser(this, API_BROWSE_IMG);
    }

    protected void showTakePicUI() {
        Utils.openImageCapture(this, API_TAKE_PIC, param);
    }

}
