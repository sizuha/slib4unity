package plugins.unity.sizuha.util.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.unity3d.player.UnityPlayerActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Map;

import plugins.unity.sizuha.util.R;


public class FcmListenerService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        final Map<String,String> data = remoteMessage.getData();
        if (!data.containsKey("message")) return;

        final String raw_data = data.get("message");
        Log.d("Push", "onMessageReceived: " + raw_data);

        PushMessage message;
        try {
            final JSONObject json = new JSONObject(raw_data);
            message = new PushMessage(json);
        }
        catch (JSONException e) {
            message = new PushMessage(raw_data);
        }

        if (message.message != null && !message.message.isEmpty()) {
            savePush(raw_data);
            showNoti(message);
        }
    }

    void savePush(String raw) {
        final String pkgName = getApplicationContext().getPackageName();
        final SharedPreferences pref = getSharedPreferences(pkgName, Context.MODE_PRIVATE);
        final int pmID = pref.getInt("last_push_cnt", 10) + 1;

        final SharedPreferences.Editor edit = pref.edit();
        edit.putInt("last_push_cnt", pmID);
        edit.putString("pm_" + pmID, raw);
        edit.commit();

        Log.d("Push", "pkg = " + pkgName);
        Log.d("Push", "save push: " + pmID + " = " + raw);
    }

    void showNoti(PushMessage message) {
        final long id = Calendar.getInstance().getTimeInMillis();
        showNoti((int)id, getApplicationContext(), "Starting 50", message);
    }

    public static void showNoti(int notiID, Context context, String title, PushMessage push) {
        final String message = push.message;

        Intent intent;
        try {
            final Class cls = Class.forName("com.google.firebase.MessagingUnityPlayerActivity");
            intent = new Intent(context, cls);
        } catch (ClassNotFoundException e) {
            intent = new Intent(context, UnityPlayerActivity.class);
        }
        final int uniqueInt = (int) (System.currentTimeMillis() & 0xfffffff);

        // Intent の作成
        final PendingIntent contentIntent = PendingIntent.getActivity(
                context, uniqueInt, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // LargeIcon の Bitmap を生成
        //final Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);

        // NotificationBuilderを作成
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context.getApplicationContext(), "default");
        builder.setContentIntent(contentIntent);
        // ステータスバーに表示されるテキスト
        builder.setTicker(message);
        // アイコン
        builder.setSmallIcon(R.drawable.ic_luancher);
        // アイコンの背景色
        builder.setColor(Color.WHITE);
        // Notificationを開いたときに表示されるタイトル
        builder.setContentTitle(title);
        // Notificationを開いたときに表示されるサブタイトル
        builder.setContentText(message);
        // Notificationを開いたときに表示されるアイコン
        //builder.setLargeIcon(largeIcon);
        // 通知するタイミング
        builder.setWhen(System.currentTimeMillis());
        // 通知時の音・バイブ・ライト
        builder.setDefaults(Notification.DEFAULT_SOUND
                | Notification.DEFAULT_VIBRATE
                | Notification.DEFAULT_LIGHTS);
        // タップするとキャンセル(消える)
        builder.setAutoCancel(false);

        Log.d("Push", "show noti");

        // NotificationManagerを取得
        final NotificationManager manager = (NotificationManager) context.getSystemService(Service.NOTIFICATION_SERVICE);
        // Notificationを作成して通知
        if (manager != null) {
            manager.notify(notiID, builder.build());
        }
    }

}
