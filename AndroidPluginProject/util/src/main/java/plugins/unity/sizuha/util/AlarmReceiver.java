package plugins.unity.sizuha.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Calendar;

import plugins.unity.sizuha.util.fcm.FcmListenerService;
import plugins.unity.sizuha.util.fcm.PushMessage;

public final class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("AlarmReceiver", "AlarmReceiver onReceived");

        final SharedPreferences pref = context.getSharedPreferences("default", Context.MODE_PRIVATE);
        final boolean enabled = pref.getBoolean("enable_noti", true);
        if (!enabled) return;

        final String[] notiDays = pref.getString("noti_day", "1,2,3,4,5,6,7").split(",");
        final int nowWeekDay = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);

        for (String day: notiDays) {
            final int weekday = Integer.parseInt(day);
            if (nowWeekDay == weekday) {
                final long id = Calendar.getInstance().getTimeInMillis();
                final PushMessage message = new PushMessage();
                message.message = "今日も英会話がんばりましょう！";

                FcmListenerService.showNoti((int)id, context, "高速記憶ENG", message);
            }
        }
    }
}
